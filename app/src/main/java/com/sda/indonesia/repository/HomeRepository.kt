package com.sda.indonesia.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.remote.RepoRepository

class HomeRepository {
    val hydraulicData = MutableLiveData<ProductModel>()
    val fittingData = MutableLiveData<ProductModel>()

    fun getListHydraulic(idCategorySub: String?,produkName: String?) {

        if (hydraulicData.value == null) {

            RepoRepository.getInstance().getListProduct(
                idCategorySub,
                produkName
            ) { isSuccess, response ->
                if (isSuccess && response != null) {
                    hydraulicData.value = response
                } else {
                    hydraulicData.value = ProductModel(emptyList(), 0, false)
                }
            }
            Log.d("aim","remote")

        } else {
            Log.d("aim","cache")
        }
    }

    fun getListFitting(idCategorySub: String?,produkName: String?) {

        if (fittingData.value == null) {

            RepoRepository.getInstance().getListProduct(
                idCategorySub,
                produkName
            ) { isSuccess, response ->
                if (isSuccess && response != null) {
                    fittingData.value = response
                } else {
                    fittingData.value = ProductModel(emptyList(), 0, false)
                }
            }
            Log.d("aim","remote")

        } else {
            Log.d("aim","cache")
        }
    }

}