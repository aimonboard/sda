package com.sda.indonesia.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.data.remote.RepoRepository

class DetailViewModel : ViewModel() {

    val productData = MutableLiveData<ProductModel>()
    val productSubData = MutableLiveData<ProductSubModel>()

    fun getProdukDetail(idProduct: String) {
        RepoRepository.getInstance().getProductDetail(idProduct) { isSuccess, response ->
            if (isSuccess && response != null) {
                productData.value = response
            } else {
                productData.value = ProductModel(emptyList(),0,false)
            }
        }
    }

    fun getProdukSub(idProduct: String) {
        RepoRepository.getInstance().getProductSub(idProduct) { isSuccess, response->
            if (isSuccess && response != null) {
                productSubData.value = response
            } else {
                productSubData.value = ProductSubModel(emptyList(),0,false)
            }
        }
    }

}
