package com.sda.indonesia.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.remote.RepoRepository

class ListViewModel : ViewModel() {

    val listProductData = MutableLiveData<ProductModel>()

    fun getListProduct(
        idCategorySub: String?,
        produkName: String?
    ) {
        RepoRepository.getInstance().getListProduct(
            idCategorySub,
            produkName
        ) { isSuccess, response->
            if (isSuccess && response != null) {
                listProductData.value = response
            } else {
                listProductData.value = ProductModel(emptyList(),0,false)
            }
        }
    }
}
