package com.sda.indonesia.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.data.remote.RepoRepository

class AssemblyFitting2VM : ViewModel() {
    val listProductData = MutableLiveData<ProductModel>()
    val productSubData = MutableLiveData<ProductSubModel>()

    fun getListProduct(
        idCategorySub: String?,
        produkName: String?
    ) {
        RepoRepository.getInstance().getListProduct(
            idCategorySub,
            produkName
        ) { isSuccess, response->
            if (isSuccess && response != null) {
                listProductData.value = response
            } else {
                listProductData.value = ProductModel(emptyList(),0,false)
            }
        }
    }

    fun getProdukSub(idProduct: String) {
        RepoRepository.getInstance().getProductSub(idProduct) { isSuccess, response->
            if (isSuccess && response != null) {
                productSubData.value = response
            } else {
                productSubData.value = ProductSubModel(emptyList(),0,false)
            }
        }
    }
}
