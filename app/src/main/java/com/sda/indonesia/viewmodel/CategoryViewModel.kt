package com.sda.indonesia.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sda.indonesia.data.model.sda.CategoryModel
import com.sda.indonesia.data.model.sda.CategorySubModel
import com.sda.indonesia.data.remote.RepoRepository

class CategoryViewModel : ViewModel() {

    val categoryData = MutableLiveData<CategoryModel>()
    val categorySubData = MutableLiveData<CategorySubModel>()

    fun getCategory() {
        RepoRepository.getInstance().getCategory { isSuccess, response ->
            if (isSuccess && response != null) {
                categoryData.value = response
            } else {
                categoryData.value = CategoryModel(emptyList(),0,false)
            }
        }
    }

    fun getCategorySub() {
        RepoRepository.getInstance().getCategorySub { isSuccess, response->
            if (isSuccess && response != null) {
                categorySubData.value = response
            } else {
                categorySubData.value = CategorySubModel(emptyList(),0,false)
            }
        }
    }
}
