package com.sda.indonesia.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sda.indonesia.data.model.rajaongkir.ShippingCostModel
import com.sda.indonesia.data.remote.RepoRepository

class DeliveryViewModel : ViewModel() {

    val listCost = MutableLiveData<ShippingCostModel>()

    fun getShippingCost(body: HashMap<String,String>) {
        val url = "https://api.rajaongkir.com/starter/cost"
        RepoRepository.getInstance().getShippingCost(url,body) { isSuccess, response->
            if (isSuccess) {
                listCost.value = response
            }
        }

    }

}
