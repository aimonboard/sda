package com.sda.indonesia.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sda.indonesia.data.model.sda.BrandModel
import com.sda.indonesia.data.model.sda.CategoryModel
import com.sda.indonesia.data.model.sda.CategorySubModel
import com.sda.indonesia.data.remote.RepoRepository

class SearchViewModel : ViewModel() {

    val categoryData = MutableLiveData<CategoryModel>()
    val categorySubData = MutableLiveData<CategorySubModel>()
    val brandData = MutableLiveData<BrandModel>()

    fun getCategory() {
        RepoRepository.getInstance().getCategory { isSuccess, response ->
            if (isSuccess && response != null) {
                categoryData.value = response
            } else {
                categoryData.value = CategoryModel(emptyList(),0,false)
            }
        }
    }

//    fun getCategorySub(idCategory: String) {
//        RepoRepository.getInstance().getCategorySub(idCategory) { isSuccess, response->
//            if (isSuccess && response != null) {
//                categorySubData.value = response
//            } else {
//                categorySubData.value = CategorySubModel(emptyList(),0,false)
//            }
//        }
//    }

    fun getBrand() {
        RepoRepository.getInstance().getBrand { isSuccess, response ->
            if (isSuccess && response != null) {
                brandData.value = response
            } else {
                brandData.value = BrandModel(emptyList(),0,false)
            }
        }
    }
}
