package com.sda.indonesia.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.remote.RepoRepository
import com.sda.indonesia.repository.HomeRepository

class HomeViewModel : ViewModel() {

    private val mRepo = HomeRepository()
    var hydraulicData = MutableLiveData<ProductModel>()
    var fittingData = MutableLiveData<ProductModel>()

    init {
        this.hydraulicData = mRepo.hydraulicData
        this.fittingData = mRepo.fittingData
    }

    fun getListHydraulic(idCategorySub: String?,produkName: String?) {
        mRepo.getListHydraulic(idCategorySub,produkName)
    }

    fun getListFitting(idCategorySub: String?,produkName: String?) {
        mRepo.getListFitting(idCategorySub,produkName)
    }







//    fun getListProduct(
//        idCategorySub: String?,
//        produkName: String?,
//        idCategory: String?,
//        idBrand: String?
//    ) {
//        RepoRepository.getInstance().getListProduct(
//            idCategorySub,
//            produkName,
//            idCategory,
//            idBrand
//        ) { isSuccess, response->
//            if (isSuccess && response != null) {
//                hydraulicData.value = response
//            } else {
//                hydraulicData.value = ProductModel(emptyList(),0,false)
//            }
//        }
//    }
}
