package com.sda.indonesia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.model.sda.ProductSubModel


class MainActivity : AppCompatActivity() {

    companion object {
        var hose: ProductModel.Data? = null
        var hoseVariant: ProductSubModel.Data? = null

        var fitting1: ProductModel.Data? = null
        var fitting1Variant: ProductSubModel.Data? = null

        var fitting2: ProductModel.Data? = null
        var fitting2Variant: ProductSubModel.Data? = null
    }

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        SharedPreferenceHelper.setSharedPreferenceString(this,11,"")
        SharedPreferenceHelper.setSharedPreferenceString(this,22,"")
        SharedPreferenceHelper.setSharedPreferenceString(this,33,"")
        SharedPreferenceHelper.setSharedPreferenceString(this,44,"")

        navController = Navigation.findNavController(this, R.id.main_nav_fragment)
        bottom_nav.setupWithNavController(navController)

        bottom_nav.setOnNavigationItemSelectedListener {
            val loginStatus = SharedPreferenceHelper.getSharedPreferenceString(this,1,"0")
            val options = NavOptions.Builder()
                .setPopUpTo(navController.currentDestination!!.id, false)
                .setLaunchSingleTop(true)
                .build()

            when (it.itemId) {
                R.id.homeFragment -> {
                    navController.navigate(R.id.homeFragment, null, options)
                    return@setOnNavigationItemSelectedListener true// return true -> we handled this navigation event
                }
                R.id.categoryFragment -> {
                    navController.navigate(R.id.categoryFragment, null, options)
                    return@setOnNavigationItemSelectedListener true// return true -> we handled this navigation event
                }
                R.id.chatFragment -> {
                    navController.navigate(R.id.contactFragment, null, options)
                    return@setOnNavigationItemSelectedListener true// return true -> we handled this navigation event
                }
                R.id.cartFragment -> {
                    navController.navigate(R.id.cartFragment, null, options)
                    return@setOnNavigationItemSelectedListener true// return true -> we handled this navigation event
                }
                R.id.profileFragment -> {
                    if (loginStatus == "1") {
                        Log.d("aim","menu click")
                        navController.navigate(R.id.profileFragment, null, options)
                    } else {
                        val bundle = Bundle()
                        bundle.putString("goto","profile")
                        navController.navigate(R.id.loginFragment, bundle, options)
                    }
                    return@setOnNavigationItemSelectedListener true// return true -> we handled this navigation event
                }
            }
             false
        }
    }
}
