package com.sda.indonesia.ui.helper

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.room.TypeConverter
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sda.indonesia.data.local.database.cart.CartConverterModel
import java.lang.reflect.Type
import java.text.NumberFormat
import java.util.*


class FunHelper {

    companion object {

        fun bank(value:Double): String {
            // Format Rupiah
            val localeID = Locale("in", "ID")
            val rupiah = NumberFormat.getCurrencyInstance(localeID)
            return rupiah.format(value).replace("Rp", "IDR ")
        }

        fun textHtml(value:String): String {
            var html = value
            html = html.replace("<(.*?)\\>".toRegex(), " ")
            html = html.replace("<(.*?)\\\n".toRegex(), " ")
            html = html.replaceFirst("(.*?)\\>".toRegex(), " ")
            html = html.replace("&nbsp;".toRegex(), " ")
            html = html.replace("&amp;".toRegex(), " ").trim()
            return  html
        }

        fun snackBar(view: View, pesan: String){
            val mSnackbar = Snackbar.make(view, pesan, Snackbar.LENGTH_LONG)
            val snackbarView = mSnackbar.view
            snackbarView.setBackgroundColor(Color.parseColor("#000000"))
            val textView = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            textView.setTextColor(Color.parseColor("#FFFFFF"))
            mSnackbar.show()
        }

        fun randomString (): String {
            return UUID.randomUUID().toString()
        }

        fun isValidEmail(email: String): Boolean {
            if (email.isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                return true
            }
            return false
        }

    }
}