package com.sda.indonesia.ui

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.sda.indonesia.R
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.data.local.database.cart.CartEntity
import com.sda.indonesia.data.local.database.cart.CartViewModelDB
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.adapter.DetailAdapter
import com.sda.indonesia.ui.adapter.FilterPressureAdapter
import com.sda.indonesia.ui.adapter.FilterSizeAdapter
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.DetailViewModel
import kotlinx.android.synthetic.main.detail_fragment.*
import kotlin.math.sqrt


class DetailFragment : Fragment() {

    private val writeStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private lateinit var viewModel: DetailViewModel
    private lateinit var cartDB: CartViewModelDB
    private val adapter = DetailAdapter()

    // Filter
    private var subProductFilter = mutableListOf<ProductSubModel.Data>()
    private val pressureModel = mutableListOf<String>()
    private val pressureAdapter = FilterPressureAdapter()
    private var pressureValue = ""

    private val sizeModel = mutableListOf<String>()
    private val sizeAdapter = FilterSizeAdapter()
    private var sizeValue = ""

    // Transaction
    private var tempData = mutableListOf<ProductSubModel.Data>()

    private var productID = ""
    private var productName = ""
    private var productCategory = ""
    private var productImage = ""
    private var totalPrice = 0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this@DetailFragment).get(DetailViewModel::class.java)
        cartDB = ViewModelProviders.of(this@DetailFragment).get(CartViewModelDB::class.java)

        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE
        header_name?.text = "Detail Product"

//        Log.d("aim", "data : $dataTransaksi")

        generateView()

        setupAdapter()
        setupObservers()
        produkListener()

        buttonListener()
    }

    private fun generateView() {
        val idProduk = requireArguments().getString("idProduk")
        Log.d("aim", "id product : $idProduk")

        idProduk?.let { viewModel.getProdukDetail(it) }
        idProduk?.let { viewModel.getProdukSub(it) }
    }

    private fun buttonListener() {
        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        btn_filter.setOnClickListener {
            dialogFilter(product_type.text.toString())
        }

        swipe_container?.setOnRefreshListener {
            setupAdapter()
            setupObservers()
        }

        product_image.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("image", productImage)
            Navigation.findNavController(it).navigate(R.id.imagePriview,bundle)
        }

        btn_download?.setOnClickListener {
            downloadCatalog()
        }

        btn_buy?.setOnClickListener {
            if (totalPrice > 0.0) {

                addToCart()

                val bundle = Bundle()
                bundle.putString("from","1")
                Navigation.findNavController(it).navigate(R.id.cartFragment,bundle)

            } else {
                FunHelper.snackBar(root_lay,"Choose at least one product")
            }
        }

        btn_cart?.setOnClickListener {
            if (totalPrice > 0.0) {

                addToCart()
                FunHelper.snackBar(root_lay,"Item added to cart")

            } else {
                FunHelper.snackBar(root_lay,"Choose at least one product")
            }
        }
    }

    private fun setupObservers() {
        swipe_container?.isRefreshing = true
        viewModel.productData.observe(viewLifecycleOwner, Observer {
            if (it.data.isNotEmpty()) {
                swipe_container?.isRefreshing = false

                productID = it.data[0].idProduk
                productName = it.data[0].namaProduk
                productCategory = it.data[0].namaKategori
                productImage = it.data[0].imgProduk

                product_desc.setShowingLine(5)
                product_desc.addShowMoreText("Read more")
                product_desc.addShowLessText("Less")
                product_desc.setShowMoreColor(Color.BLUE) // or other color
                product_desc.setShowLessTextColor(Color.BLUE) // or other color

                product_name?.text = productName
                product_desc?.text = FunHelper.textHtml(it.data[0].deskripsiProduk)
                product_type?.text = it.data[0].produkType
                product_category?.text = it.data[0].namaKategori

                try {
                    Glide.with(this).load(productImage).into(product_image)
                    Glide.with(this).load(it.data[0].imgBrand).into(product_brand)
                } catch (e: Exception) {  }

            } else {
                swipe_container?.isRefreshing = false

                product_name?.text = ""
                product_desc?.text = ""
    //                product_brand?.text = ""
                product_type?.text = ""
                product_category?.text = ""
            }
        })

        viewModel.productSubData.observe(viewLifecycleOwner, Observer { data ->
            if (data.status) {
                swipe_container?.isRefreshing = false
                adapter.setList(product_type.text.toString(), data.data)

                subProductFilter.clear()
                pressureModel.clear()
                sizeModel.clear()

                subProductFilter = data.data.toMutableList()
                for (element in data.data) {

                    sizeModel.add(element.size ?: "")

                    val pressure = element.pressure ?: ""
                    if (pressure.isNotEmpty()) pressureModel.add(element.pressure ?: "")

                }

            } else {
                swipe_container?.isRefreshing = false
                adapter.setList(product_type.text.toString(), emptyList())
            }
        })
    }

    private fun setupAdapter() {

        val metrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(metrics)

        val yInches: Float = metrics.heightPixels / metrics.ydpi
        val xInches: Float = metrics.widthPixels / metrics.xdpi
        val diagonalInches = sqrt(xInches * xInches + yInches * yInches.toDouble())

        if (diagonalInches >= 7){
            recycler_sub_product?.adapter = adapter
            recycler_sub_product?.layoutManager = GridLayoutManager(activity,2)
        } else {
            recycler_sub_product?.adapter = adapter
            recycler_sub_product?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        }
    }

    private fun produkListener() {
        adapter.setEventHandler(object: DetailAdapter.RecyclerClickListener {
            override fun isClicked(price: Double, data: MutableList<ProductSubModel.Data>) {

                tempData = data

                totalPrice = price
                total_value.text = FunHelper.bank(price)

            }

        })
    }

    private fun addToCart() {
        tempData.forEach {

            val mCart = CartEntity(
                "$productID${it.idSubProduk}",
                null,
                productID,
                productName,
                productCategory,
                productImage,
                it.idSubProduk,
                it.dash?: "",
                "",
                it.size?: "",
                it.namesize?: "",
                it.length?: "",
                it.namelength?: "",
                it.pressure?: "",
                "",
                it.weight,
                it.nameweight,
                it.hargaProduk,
                "",
                it.stokProduk,
                ""
            )
            cartDB.insert(mCart)

        }
    }

    private fun downloadCatalog() = runWithPermissions(writeStorage) {

        val url = "https://drive.google.com/open?id=1YGXXHbdAj0mh70tMXOKJzbTs4HM34pp8"

        val request = DownloadManager.Request(Uri.parse(url))
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI)
        request.setTitle("Download Catalog")
        request.setDescription("downloading ...")

        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,"SDA Catalog")

        //get download service and enqueue file
        val manager = activity?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        manager.enqueue(request)
    }

    private fun dialogFilter(productType: String) {

        listenerPressure()
        listenerSize()

        val view = layoutInflater.inflate(R.layout.dialog_filter,null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)

        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val sizeFlex = FlexboxLayoutManager(context)
        sizeFlex.flexDirection = FlexDirection.ROW
        sizeFlex.flexWrap = FlexWrap.WRAP

        val recyclerSize = dialog.findViewById<RecyclerView>(R.id.recycler_size)
        recyclerSize?.layoutManager = sizeFlex
        recyclerSize?.adapter = sizeAdapter
        sizeAdapter.setList(sizeValue, sizeModel)

        val dashFlex = FlexboxLayoutManager(context)
        dashFlex.flexDirection = FlexDirection.ROW
        dashFlex.flexWrap = FlexWrap.WRAP
        val textPressure = dialog.findViewById<TextView>(R.id.tx_pressure)
        val recyclerDash = dialog.findViewById<RecyclerView>(R.id.recycler_pressure)

        if (pressureModel.isNotEmpty()) {
            textPressure?.visibility = View.VISIBLE
            recyclerDash?.visibility = View.VISIBLE
            recyclerDash?.layoutManager = dashFlex
            recyclerDash?.adapter = pressureAdapter
            pressureAdapter.setList(pressureValue, pressureModel)
        } else {
            textPressure?.visibility = View.GONE
            recyclerDash?.visibility = View.GONE
        }

        val btnApply = dialog.findViewById<TextView>(R.id.btn_apply)
        val btnReset = dialog.findViewById<TextView>(R.id.btn_reset)

        btnApply?.setOnClickListener {
            val filterResult = subProductFilter.filter {
                it.pressure!!.contains(pressureValue,true)  && it.size!!.contains(sizeValue, true)
            }
            adapter.setList(productType, filterResult)

            if (filterResult.isEmpty()) {
                tx_filter.visibility = View.VISIBLE
            } else {
                tx_filter.visibility = View.GONE
            }

            dialog.dismiss()
        }

        btnReset?.setOnClickListener {
            tx_filter.visibility = View.GONE
            pressureValue = ""
            sizeValue = ""

            adapter.setList(productType, subProductFilter)
            dialog.dismiss()
        }



        dialog.show()
    }

    private fun listenerSize() {
        sizeAdapter.setEventHandler(object: FilterSizeAdapter.RecyclerClickListener {
            override fun isClicked(name: String) {
                sizeValue = name
            }
        })
    }
    private fun listenerPressure() {
        pressureAdapter.setEventHandler(object: FilterPressureAdapter.RecyclerClickListener {
            override fun isClicked(name: String) {
                pressureValue = name
            }
        })
    }



}
