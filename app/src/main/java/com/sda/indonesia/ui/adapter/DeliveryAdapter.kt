package com.sda.indonesia.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.DeliveryModel
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_delivery.view.*

class DeliveryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<DeliveryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_delivery, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])


    }

    fun setList(listOfVendor: List<DeliveryModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: DeliveryModel) {

            itemView.product_name?.text = "${vendorModel.productName} (${vendorModel.subQty})"
            itemView.sub_code.text = "iyi7y776g"
            itemView.sub_price.text = FunHelper.bank(vendorModel.subPrice)
        }
    }

}