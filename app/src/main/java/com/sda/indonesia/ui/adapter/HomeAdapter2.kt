package com.sda.indonesia.ui.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.DataRepo
import kotlinx.android.synthetic.main.item_product_horizontal.view.*

class HomeAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<DataRepo.DataMain.Data>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_product_horizontal, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder

        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener { view ->

            val id = vendorList[position].id.toString()
            val bundle = Bundle()
            bundle.putString("idProduk",id)
            Navigation.findNavController(view).navigate(R.id.detailFragment,bundle)

        }
    }
    fun setList(listOfVendor: List<DataRepo.DataMain.Data>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<DataRepo.DataMain.Data>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: DataRepo.DataMain.Data) {

            itemView.tx_name.text = vendorModel.nama

            Glide.with(itemView.context)
                .load(vendorModel.imagePath)
                .into(itemView.photo)
        }
    }
}