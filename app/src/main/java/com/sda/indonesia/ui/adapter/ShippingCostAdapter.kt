package com.sda.indonesia.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.rajaongkir.ShippingCostModel
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_shipping_cost.view.*

class ShippingCostAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ShippingCostModel.Rajaongkir.Result.Data>()
    private var eventHandler: RecyclerClickListener? = null
    private val itemStateArray = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_shipping_cost, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        val idCategory = vendorList[position].service

        holder.itemView.setOnClickListener { view ->

            notifyDataSetChanged()
            itemStateArray.clear()

            if (!itemStateArray.contains(idCategory)) {
                itemStateArray.add(idCategory)
                val name = vendorList[position].service
                val estimate = vendorList[position].cost[0].etd
                val price = vendorList[position].cost[0].value
                eventHandler?.isClicked("$name ($estimate Day)",price)
            } else {
                itemStateArray.remove(idCategory)
            }
        }

        if (itemStateArray.contains(idCategory)) {
            holder.itemView.root_lay.setBackgroundResource(R.color.dark_gray)
        } else {
            holder.itemView.root_lay.setBackgroundResource(0x00000000)
        }

    }

    fun setList(listOfVendor: List<ShippingCostModel.Rajaongkir.Result.Data>) {
        this.vendorList = listOfVendor.toMutableList()
        itemStateArray.clear()
        notifyDataSetChanged()

        itemStateArray.add(vendorList[0].service)
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ShippingCostModel.Rajaongkir.Result.Data) {
            val nama = vendorModel.service
            val estimasi = if (vendorModel.cost[0].etd == "1-1") {
                "1"
            } else {
                vendorModel.cost[0].etd
            }

            itemView.name_value.text = "$nama ($estimasi Day)"
            itemView.desc_value.text = vendorModel.description
            itemView.price_value.text = FunHelper.bank(vendorModel.cost[0].value)
        }
    }


    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(name: String, price: Double)
    }

}