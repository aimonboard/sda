package com.sda.indonesia.ui

import android.os.Bundle
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.RegisterModel
import com.sda.indonesia.data.remote.ApiClient
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.RegisterViewModel
import kotlinx.android.synthetic.main.register_fragment.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegisterFragment : Fragment() {

    private lateinit var viewModel: RegisterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)

        terms.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("url", "http://192.168.1.93/xxx/p/termsmobile")
            Navigation.findNavController(requireView()).navigate(R.id.action_registerFragment_to_privacyPolicy, bundle)
        }

        privacy.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("url", "http://192.168.1.93/xxx/p/privacymobile")
            Navigation.findNavController(requireView()).navigate(R.id.action_registerFragment_to_privacyPolicy, bundle)
        }

        btnRegister.setOnClickListener {
            if (emptyCheck()) {
                if (check_box.isChecked) {
                    register()
                } else {
                    FunHelper.snackBar(it, "You must accept the Terms and Conditions.")
                }
            }
        }

    }

    private fun emptyCheck(): Boolean{
        var cek = 0

        if(name.text.isEmpty()){
            name_lay.error = "Required"; cek++
        } else { name_lay.error = "" }

        if(email.text.isEmpty()){
            email_lay.error = "Required"; cek++
        } else { email_lay.error = "" }

        if(pass.text.isEmpty()){
            pass_lay.error = "Required"; cek++
        } else { pass_lay.error = "" }

        if(confirm_pass.text.isEmpty()){
            confirm_pass_lay.error = "Required"; cek++
        } else { confirm_pass_lay.error = "" }

        if(FunHelper.isValidEmail(email.text.toString())){
            email_lay.error = ""
        } else { email_lay.error = "Email not valid"; cek++ }

        if(pass.text.toString() != confirm_pass.text.toString()){
            confirm_pass_lay.error = "Not match"; cek++
        } else { confirm_pass_lay.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun register() {
        loading.visibility = View.VISIBLE
//        btnRegister.isEnabled = false

        val params = HashMap<String, String>()
        params["nama"] = name.text.toString()
        params["email"] = email.text.toString()
        params["password"] = pass.text.toString()

        ApiClient.instance.registerRequest(params).enqueue(object: Callback<RegisterModel> {
            override fun onResponse(call: Call<RegisterModel>, response: Response<RegisterModel>?) {
                if (response != null && response.isSuccessful) {
                    FunHelper.snackBar(root_lay, "Register success")
                    requireActivity().onBackPressed()
                }
                else {
                    try {
                        val jObjError = JSONObject(response!!.errorBody()!!.string())
                        FunHelper.snackBar(root_lay, jObjError.getString("message"))
                    } catch (e: Exception) { }
                }
                loading.visibility = View.GONE
//                btnRegister.isEnabled = true
            }

            override fun onFailure(call: Call<RegisterModel>, t: Throwable) {
                loading.visibility = View.GONE
//                btnRegister.isEnabled = true
            }

        })
    }

}
