package com.sda.indonesia.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sda.indonesia.MainActivity

import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.adapter.AssemblyHoseProductSub
import com.sda.indonesia.ui.adapter.AssemblyHoseProduct
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.AssemblyHoseVM
import kotlinx.android.synthetic.main.assembly_hose_fragmnet.*
import kotlin.math.sqrt

class AssemblyHoseFragmnet : Fragment() {

    private lateinit var viewModel: AssemblyHoseVM
    private val adapterHose = AssemblyHoseProduct()
    private val adapterDiameter = AssemblyHoseProductSub()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.assembly_hose_fragmnet, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AssemblyHoseVM::class.java)
        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        back_link.setOnClickListener {
            requireActivity().onBackPressed()
        }

        viewModel.getListProduct("1",null,null,null)

        setupAdapter()
        setupObservers()

        spinnerLength()

        listenerProduct()
        listenerProductSub()

        btn_hose.setOnClickListener {
            if (recycler_hose.visibility == View.VISIBLE) {
                recycler_hose.visibility = View.GONE
                btn_hose.rotation = 180f
            } else {
                recycler_hose.visibility = View.VISIBLE
                btn_hose.rotation = 0f
            }
        }

        btn_diameter.setOnClickListener {
            if (recycler_diameter.visibility == View.VISIBLE) {
                recycler_diameter.visibility = View.GONE
                btn_diameter.rotation = 180f
            } else {
                recycler_diameter.visibility = View.VISIBLE
                btn_diameter.rotation = 0f
            }
        }

        btn_length.setOnClickListener {
            if (length_lay.visibility == View.VISIBLE) {
                length_lay.visibility = View.GONE
                btn_length.rotation = 180f
            } else {
                length_lay.visibility = View.VISIBLE
                btn_length.rotation = 0f
            }
        }

        btn_next.setOnClickListener {
            if (MainActivity.hose != null && MainActivity.hoseVariant != null) {
                Navigation.findNavController(it).navigate(R.id.assemblyFitting1Fragment)
            } else {
                FunHelper.snackBar(it, "Please complete your order information")
            }
        }


    }

    private fun setupObservers() {
//        swipe_container?.isRefreshing = true
        viewModel.listProductData.observe(viewLifecycleOwner, Observer {
            if (it.data.isNotEmpty()) {
                hose_loading.visibility = View.GONE
                adapterHose.setList(it.data)
            } else {
                hose_loading.visibility = View.VISIBLE
                adapterHose.setList(emptyList())
                Toast.makeText(activity, "Empty Data", Toast.LENGTH_LONG).show()
            }
        })

        viewModel.productSubData.observe(viewLifecycleOwner, Observer {
            if (it.status) {
                diameter_loading.visibility = View.GONE
                adapterDiameter.setList(it.data)
            } else {
                diameter_loading.visibility = View.GONE
                adapterDiameter.setList(emptyList())
            }
        })
    }

    private fun setupAdapter() {
        val metrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(metrics)

        val yInches: Float = metrics.heightPixels / metrics.ydpi
        val xInches: Float = metrics.widthPixels / metrics.xdpi
        val diagonalInches = sqrt(xInches * xInches + yInches * yInches.toDouble())

        if (diagonalInches >= 7){
            recycler_hose?.layoutManager = GridLayoutManager(activity, 3)
            recycler_hose?.adapter = adapterHose

            recycler_diameter?.layoutManager = GridLayoutManager(activity, 4)
            recycler_diameter?.adapter = adapterDiameter
        } else {
            recycler_hose?.layoutManager = GridLayoutManager(activity, 2)
            recycler_hose?.adapter = adapterHose

            recycler_diameter?.layoutManager = GridLayoutManager(activity, 3)
            recycler_diameter?.adapter = adapterDiameter
        }
    }

    private fun spinnerLength(){
        val spinnerData = arrayOf("Cm","M")
        val arrayAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line, spinnerData)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        length_spinner?.adapter = arrayAdapter

        length_spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (spinnerData[position]){
                    "Cm" -> {

                    }
                    "M" -> {

                    }
                }
            }
        }
    }

    private fun listenerProduct() {
        adapterHose.setEventHandler(object: AssemblyHoseProduct.RecyclerClickListener {
            override fun isClicked(data: ProductModel.Data) {
                diameter_loading.visibility = View.VISIBLE
                viewModel.getProdukSub(data.idProduk)
                MainActivity.hose = data

//                recycler_hose.visibility = View.GONE
//                btn_hose.rotation = 180f

                scroll_lay.scrollY = btn_diameter.top

            }
        })
    }

    private fun listenerProductSub() {
        adapterDiameter.setEventHandler(object: AssemblyHoseProductSub.RecyclerClickListener {
            override fun isClicked(data: ProductSubModel.Data) {
                MainActivity.hoseVariant = data

//                recycler_diameter.visibility = View.GONE
//                btn_diameter.rotation = 180f

                scroll_lay.scrollY = btn_length.top
            }

        })
    }

}
