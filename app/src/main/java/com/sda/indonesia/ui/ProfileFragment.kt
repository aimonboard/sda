package com.sda.indonesia.ui

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Button
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.observe
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputLayout
import com.sda.indonesia.MainActivity

import com.sda.indonesia.R
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.data.remote.EndPoint
import com.sda.indonesia.viewmodel.ProfileViewModel
import kotlinx.android.synthetic.main.profile_fragment.*
import org.json.JSONException
import org.json.JSONObject

class ProfileFragment : Fragment() {

    lateinit var appContext: Context
    lateinit var viewContext: Context

    lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        appContext = requireActivity().applicationContext
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewContext = view.context

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(EndPoint.oAuthWeb)
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)

        val navBar = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar.visibility = View.VISIBLE


        val img = "https://sdaindonesia.com/beta/assets/img/logo.png"
        try { Glide.with(viewContext).load(img).into(profile_photo) }
        catch (e: Exception) {  }

        profile_name.text = SharedPreferenceHelper.getSharedPreferenceString(context,4, "")

        buttonListener()


//        requireActivity().onBackPressedDispatcher.addCallback(this,object: OnBackPressedCallback(true) {
//            override fun handleOnBackPressed() {
//                Log.d("aim","test back from fragment")
////                Navigation.findNavController(view).previousBackStackEntry?.savedStateHandle?.set("backStack", "0")
//                Navigation.findNavController(view).popBackStack()
//            }
//
//        })


    }

    private fun buttonListener() {

        trans_lay.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.transaction)
        }

        logout_lay.setOnClickListener {
            SharedPreferenceHelper.setSharedPreferenceString(context,1,"0")
            signOut()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
    }

    private fun signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener {
            if (it.isComplete) {
                val intent = Intent(context, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                activity?.finish()
            }
        }
    }
}
