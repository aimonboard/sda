package com.sda.indonesia.ui

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView

import com.sda.indonesia.R
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.data.local.database.city.CityViewModel
import com.sda.indonesia.data.local.database.province.ProvinceViewModel
import com.sda.indonesia.data.model.rajaongkir.CityModel
import com.sda.indonesia.data.model.rajaongkir.ProvinceModel
import com.sda.indonesia.ui.adapter.CityAdapter
import com.sda.indonesia.ui.adapter.ProvinceAdapter
import kotlinx.android.synthetic.main.address_fragment.*
import kotlinx.android.synthetic.main.sub_header.*

class AddressFragment : Fragment() {

    private lateinit var provinceDB: ProvinceViewModel
    private lateinit var cityDB : CityViewModel

    private val provinceAdapter = ProvinceAdapter()
    private val cityAdapter = CityAdapter()

    private val provinceData = mutableListOf<ProvinceModel.Rajaongkir.Result>()
    private val provinceFilter = mutableListOf<ProvinceModel.Rajaongkir.Result>()

    private val cityData = mutableListOf<CityModel.Rajaongkir.Result>()
    private val cityFilter = mutableListOf<CityModel.Rajaongkir.Result>()

    private var selectedProvince = ""
    private var selectedCity = ""

    private var cityID = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.address_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        provinceDB = ViewModelProviders.of(this).get(ProvinceViewModel::class.java)
        cityDB = ViewModelProviders.of(this).get(CityViewModel::class.java)

        header_name?.text = "Shipping Address"

        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        generateView()
        btnListener()

        getProvince()
        provinceListener()

        getCity()
        cityListener()
    }

    private fun generateView() {
        val isEdit = requireArguments().getBoolean("edit")
        if (isEdit) {
            alamat_value.setText(SharedPreferenceHelper.getSharedPreferenceString   (activity,111,""))
            pos_value.setText(SharedPreferenceHelper.getSharedPreferenceString      (activity,222,""))
            city_value.setText(SharedPreferenceHelper.getSharedPreferenceString     (activity,333,""))
            province_value.setText(SharedPreferenceHelper.getSharedPreferenceString (activity,555,""))


            nama_value.setText(SharedPreferenceHelper.getSharedPreferenceString     (activity,1111,""))
            telepon_value.setText(SharedPreferenceHelper.getSharedPreferenceString  (activity,2222,""))
        }
    }

    private fun btnListener() {
        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        btn_apply.setOnClickListener {
            if (checkInput()) {
                SharedPreferenceHelper.setSharedPreferenceString(context,111,alamat_value.text.toString().capitalize())
                SharedPreferenceHelper.setSharedPreferenceString(context,222,pos_value.text.toString())
                SharedPreferenceHelper.setSharedPreferenceString(context,333,city_value.text.toString().capitalize())
                SharedPreferenceHelper.setSharedPreferenceString(context,444,cityID)
                SharedPreferenceHelper.setSharedPreferenceString(context,555,province_value.text.toString().capitalize())

                SharedPreferenceHelper.setSharedPreferenceString(context,1111,nama_value.text.toString().capitalize())
                SharedPreferenceHelper.setSharedPreferenceString(context,2222,telepon_value.text.toString())

                Navigation.findNavController(it).popBackStack()
            }
        }
    }

    private fun checkInput(): Boolean{
        var cek = 0

        if (province_value.text!!.isEmpty()) {
            province_lay.error = "Can't be empty"; cek++
        } else { province_lay.error = "" }
        if (selectedProvince.isEmpty()) {
            province_lay.error = "Invalid province, choose from available list"; cek++
        }else { province_lay.error = "" }

        if (city_value.text!!.isEmpty()) {
            city_lay.error = "Can't be empty"; cek++
        } else { city_lay.error = "" }
        if (selectedCity.isEmpty()) {
            city_lay.error = "Invalid city, choose from available list"; cek++
        }else { city_lay.error = "" }

        if (alamat_value.text!!.isEmpty()) {
            alamat_lay.error = "Can't be empty"; cek++
        } else { alamat_lay.error = "" }

        if (pos_value.text!!.isEmpty()) {
            pos_lay.error = "Can't be empty"; cek++
        } else { pos_lay.error = "" }

        if(nama_value.text!!.isEmpty()) {
            nama_lay.error = "Can't be empty"; cek++
        } else { nama_lay.error = "" }

        if(telepon_value.text!!.isEmpty()) {
            telepon_lay.error = "Can't be empty"; cek++
        } else { telepon_lay.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }



    private fun getProvince() {
        provinceDB.getAll().observe(viewLifecycleOwner, Observer { data ->
            data.forEach {
                val mProvince = ProvinceModel.Rajaongkir.Result(it.name,it.id)
                provinceData.add(mProvince)
            }

            recycler_province?.layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
            recycler_province?.adapter = provinceAdapter
            provinceAdapter.setList(provinceData)


//            if (categoryDB.getAll().hasActiveObservers()) {
//                categoryDB.getAll().removeObservers(this)
//            }
        })
    }

    private fun provinceListener() {
        recycler_province?.layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
        recycler_province?.adapter = provinceAdapter

        province_value?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                provinceFilter.clear()
                selectedProvince = ""

                cityFilter.clear()
                city_value.setText("")
                selectedCity = ""

                for (i in 0 until provinceData.size) {
                    if (provinceData[i].province.contains(province_value.text.toString(),true)) {
                        if (provinceFilter.size <= 3) {
                            provinceFilter.add(provinceData[i])
                        }
                    }
                }

                if (provinceFilter.isEmpty() || province_value.text.toString().isEmpty()) {
                    recycler_province.visibility = View.GONE
                }
                else {
                    recycler_province.visibility = View.VISIBLE
                    provinceAdapter.setList(provinceFilter)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                selectedProvince = province_value.text.toString()
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        provinceAdapter.setEventHandler(object: ProvinceAdapter.RecyclerClickListener {
            override fun isClicked(id: String, name: String) {
                Log.d("aim","province : $id")

                city_value?.setText("")

                province_lay.error = ""
                province_value?.setText(name)
                selectedProvince = name
                province_value?.clearFocus()
                recycler_province?.visibility = View.GONE
            }
        })
    }



    private fun getCity() {
        cityDB.getAll().observe(viewLifecycleOwner, Observer { data ->
            data.forEach {
                val mCity = CityModel.Rajaongkir.Result(it.city_id,it.city_name,it.postal_code,it.province,it.province_id,it.type)
                cityData.add(mCity)
            }

            recycler_city?.layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
            recycler_city?.adapter = cityAdapter
            cityAdapter.setList(cityData)


//            if (categoryDB.getAll().hasActiveObservers()) {
//                categoryDB.getAll().removeObservers(this)
//            }
        })
    }

    private fun cityListener() {
        recycler_city?.layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
        recycler_city?.adapter = provinceAdapter

        city_value?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                cityFilter.clear()
                selectedCity = ""

                for (i in 0 until cityData.size) {
                    if (cityData[i].cityName.contains(city_value?.text.toString(),true)) {
                        if (cityFilter.size <= 3 && cityData[i].province.contains(province_value?.text.toString(),true)) {
                            cityFilter.add(cityData[i])
                        }
                    }
                }

                if (cityFilter.isEmpty() || city_value.text.toString().isEmpty()) {
                    recycler_city.visibility = View.GONE
                }
                else {
                    recycler_city.visibility = View.VISIBLE
                    cityAdapter.setList(cityFilter)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        cityAdapter.setEventHandler(object: CityAdapter.RecyclerClickListener {
            override fun isClicked(id: String, name: String) {
                Log.d("aim","city : $id")

                cityID = id
                city_lay.error = ""
                city_value?.setText(name)
                selectedCity = name
                city_value?.clearFocus()
                recycler_city?.visibility = View.GONE
            }
        })

    }
}
