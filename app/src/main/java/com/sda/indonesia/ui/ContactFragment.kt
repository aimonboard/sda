package com.sda.indonesia.ui


import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.sda.indonesia.R
import kotlinx.android.synthetic.main.fragment_contact.*
import kotlinx.android.synthetic.main.sub_header.*


class ContactFragment : Fragment() {

    private var phone_permission = Manifest.permission.CALL_PHONE

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        header_name?.text = "Contact"

        val navBar = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar.visibility = View.VISIBLE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollListener()
        }

        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        email1.setOnClickListener {
            val mail = email1_value.text.toString()
            sendEmail(mail)
        }
        email2.setOnClickListener {
            val mail = email2_value.text.toString()
            sendEmail(mail)
        }
        email3.setOnClickListener {
            val mail = email3_value.text.toString()
            sendEmail(mail)
        }

        call1.setOnClickListener {
            val telepon = call1_value.text.toString().replace(" ","").replace("-","")
            phoneCall(telepon)
        }
        call2.setOnClickListener {
            val telepon = call2_value.text.toString().replace(" ","").replace("-","")
            phoneCall(telepon)
        }
        call3.setOnClickListener {
            val telepon = call3_value.text.toString().replace(" ","").replace("-","")
            phoneCall(telepon)
        }

        direction1.setOnClickListener {
            val map = "http://maps.google.co.in/maps?q=" + direction1_value.text.toString()
            val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
            startActivity(i)
        }
        direction2.setOnClickListener {
            val map = "http://maps.google.co.in/maps?q=" + direction2_value.text.toString()
            val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
            startActivity(i)
        }
        direction3.setOnClickListener {
            val map = "http://maps.google.co.in/maps?q=" + direction3_value.text.toString()
            val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
            startActivity(i)
        }

        fab.isIconAnimated = false

        fab_sales.setOnClickListener {
            openWhatsApp("+6281220888800")
        }

        fab_technical.setOnClickListener {
            openWhatsApp("+6281231587018")
        }



    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun scrollListener() {
        scroll_lay.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            Log.d("aim", "new = $scrollY,old x = $oldScrollY")
            if (scrollY > oldScrollY) {
                fab.visibility = View.GONE
            } else if (scrollY < oldScrollY) {
                fab.visibility = View.VISIBLE
            }
        }
    }

    private fun sendEmail (email: String) {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        try {
            startActivity(Intent.createChooser(i, "Send E-mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun phoneCall(phone: String) = runWithPermissions(phone_permission) {
        try {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$phone")
            startActivity(intent)
        }catch (e: Exception) {
            Toast.makeText(activity,"Data not available",Toast.LENGTH_LONG).show()
        }
    }

    private fun openWhatsApp(telepon: String) {
        try {
            val url = "https://api.whatsapp.com/send?phone=$telepon"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        } catch (e: Exception) {
            Log.d("aim", "open wa : $e")
        }
    }


}
