package com.sda.indonesia.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sda.indonesia.MainActivity

import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.adapter.AssemblyFitting1Product
import com.sda.indonesia.ui.adapter.AssemblyFitting1ProductSub
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.AssemblyFitting1VM
import kotlinx.android.synthetic.main.assembly_fitting1_fragment.*
import kotlin.math.sqrt

class AssemblyFitting1Fragment : Fragment() {

    private lateinit var viewModel: AssemblyFitting1VM
    private val productAdapter = AssemblyFitting1Product()
    private val productSubAdapter = AssemblyFitting1ProductSub()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.assembly_fitting1_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AssemblyFitting1VM::class.java)
        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        back_link.setOnClickListener {
            requireActivity().onBackPressed()
        }

        viewModel.getListProduct("2",null)

        setupAdapter()
        setupObservers()

        listenerProduct()
        listenerProductSub()

        btn_fitting1.setOnClickListener {
            if (recycler_fitting1.visibility == View.VISIBLE) {
                recycler_fitting1.visibility = View.GONE
                btn_fitting1.rotation = 180f
            } else {
                recycler_fitting1.visibility = View.VISIBLE
                btn_fitting1.rotation = 0f
            }
        }

        btn_fitting2.setOnClickListener {
            if (recycler_variant.visibility == View.VISIBLE) {
                recycler_variant.visibility = View.GONE
                btn_fitting2.rotation = 180f
            } else {
                recycler_variant.visibility = View.VISIBLE
                btn_fitting2.rotation = 0f
            }
        }

        btn_angle.setOnClickListener {
            if (angle_lay.visibility == View.VISIBLE) {
                angle_lay.visibility = View.GONE
                btn_angle.rotation = 180f
            } else {
                angle_lay.visibility = View.VISIBLE
                btn_angle.rotation = 0f
            }
        }

        btn_prev.setOnClickListener {
            requireActivity().onBackPressed()
        }

        btn_next.setOnClickListener {
            if (MainActivity.fitting1 != null && MainActivity.fitting1Variant != null) {
                Navigation.findNavController(it).navigate(R.id.assemblyFitting2Fragment)
            } else {
                FunHelper.snackBar(it, "Please complete your order information")
            }
        }

    }

    private fun setupObservers() {
//        swipe_container?.isRefreshing = true
        viewModel.listProductData.observe(viewLifecycleOwner, Observer {
            if (it.data.isNotEmpty()) {
                fitting1_loading.visibility = View.GONE
                productAdapter.setList(it.data)
            } else {
                fitting1_loading.visibility = View.GONE
                productAdapter.setList(emptyList())
                Toast.makeText(activity, "Empty Data", Toast.LENGTH_LONG).show()
            }
        })

        viewModel.productSubData.observe(viewLifecycleOwner, Observer {
            if (it.status) {
                variant_loading.visibility = View.GONE
                productSubAdapter.setList(it.data)
            } else {
                variant_loading.visibility = View.GONE
                productSubAdapter.setList(emptyList())
            }
        })
    }

    private fun setupAdapter() {
        val metrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(metrics)

        val yInches: Float = metrics.heightPixels / metrics.ydpi
        val xInches: Float = metrics.widthPixels / metrics.xdpi
        val diagonalInches = sqrt(xInches * xInches + yInches * yInches.toDouble())

        if (diagonalInches >= 7){
            recycler_fitting1?.layoutManager = GridLayoutManager(activity,3)
            recycler_fitting1?.adapter = productAdapter

            recycler_variant?.layoutManager = GridLayoutManager(activity,4)
            recycler_variant?.adapter = productSubAdapter
        } else {
            recycler_fitting1?.layoutManager = GridLayoutManager(activity,2)
            recycler_fitting1?.adapter = productAdapter

            recycler_variant?.layoutManager = GridLayoutManager(activity,3)
            recycler_variant?.adapter = productSubAdapter
        }
    }

    private fun listenerProduct() {
        productAdapter.setEventHandler(object: AssemblyFitting1Product.RecyclerClickListener {
            override fun isClicked(data: ProductModel.Data) {
                variant_loading.visibility = View.VISIBLE
                viewModel.getProdukSub(data.idProduk)
                MainActivity.fitting1 = data

//                recycler_fitting1.visibility = View.GONE
//                btn_fitting1.rotation = 180f

                scroll_lay.scrollY = btn_fitting2.top
            }
        })
    }

    private fun listenerProductSub() {
        productSubAdapter.setEventHandler(object: AssemblyFitting1ProductSub.RecyclerClickListener {
            override fun isClicked(data: ProductSubModel.Data) {
                MainActivity.fitting1Variant = data

//                recycler_variant.visibility = View.GONE
//                btn_fitting2.rotation = 180f

                scroll_lay.scrollY = btn_angle.top
            }

        })
    }

}
