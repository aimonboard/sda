package com.sda.indonesia.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

import com.sda.indonesia.R
import com.sda.indonesia.data.local.database.category.CategoryEntity
import com.sda.indonesia.data.local.database.category.CategoryViewModelDB
import com.sda.indonesia.data.local.database.categorysub.CategorySubEntity
import com.sda.indonesia.data.local.database.categorysub.CategorySubViewModelDB
import com.sda.indonesia.data.local.database.city.CityEntity
import com.sda.indonesia.data.local.database.city.CityViewModel
import com.sda.indonesia.data.local.database.province.ProvinceEntity
import com.sda.indonesia.data.local.database.province.ProvinceViewModel
import com.sda.indonesia.data.remote.RepoRepository
import kotlinx.android.synthetic.main.splash_screen_fragment.*

class SplashScreen : Fragment() {

    private lateinit var categoryDB: CategoryViewModelDB
    private lateinit var categorySubDB: CategorySubViewModelDB

    private lateinit var provinceDB: ProvinceViewModel
    private lateinit var cityDB : CityViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.splash_screen_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryDB = ViewModelProviders.of(this@SplashScreen).get(CategoryViewModelDB::class.java)
        categorySubDB = ViewModelProviders.of(this@SplashScreen).get(CategorySubViewModelDB::class.java)

        provinceDB = ViewModelProviders.of(this@SplashScreen).get(ProvinceViewModel::class.java)
        cityDB = ViewModelProviders.of(this@SplashScreen).get(CityViewModel::class.java)

        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        cacheCategory()
        cacheCategorySub()

        cacheProvince()
        cacheCity()

        val myFadeInAnimation = AnimationUtils.loadAnimation(activity, R.anim.anim_splash_screen)
        logo.startAnimation(myFadeInAnimation)

        Handler().postDelayed({
            findNavController().navigate(R.id.action_splashScreen_to_homeFragment)
        }, 3000)
    }

    private fun cacheCategory() {
        RepoRepository.getInstance().getCategory { isSuccess, response ->
            if (isSuccess) {
                if (!response?.data.isNullOrEmpty()) {
//                    categoryDB.deleteAll()

                    Log.d("aim","category : $response")
                    response?.data?.forEach { model ->
                        val mCategory = CategoryEntity(model.idKategori,model.namaKategori,model.imgIcon)
                        categoryDB.insert(mCategory)
                    }
                }
            }
        }
    }

    private fun cacheCategorySub() {
        RepoRepository.getInstance().getCategorySub { isSuccess, response ->
            if (isSuccess) {
                if (!response?.data.isNullOrEmpty()) {
//                    categorySubDB.deleteAll()

                    Log.d("aim","sub category : $response")
                    response?.data?.forEach {
                        val mCategorySub = CategorySubEntity(it.idKategoriSub,it.idKategori,it.namaKategoriSub,it.imgProduk)
                        categorySubDB.insert(mCategorySub)
                    }
                }
            }
        }
    }

    private fun cacheProvince() {
        val url = "https://api.rajaongkir.com/starter/province"
        RepoRepository.getInstance().getProvince(url) { isSuccess, response ->
            if (isSuccess) {
                response?.rajaongkir?.results?.forEach {
                    val mProvince = ProvinceEntity(it.provinceId,it.province)
                    provinceDB.insert(mProvince)
                }
            }
        }
    }

    private fun cacheCity() {
        val url = "https://api.rajaongkir.com/starter/city"
        RepoRepository.getInstance().getCity(url) { isSuccess, response ->
            if (isSuccess) {
                response?.rajaongkir?.results?.forEach {
                    val mCity = CityEntity(it.cityId,it.provinceId,it.province,it.type,it.cityName,it.postalCode)
                    cityDB.insert(mCity)
                }
            }
        }
    }
}
