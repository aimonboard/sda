package com.sda.indonesia.ui.adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_diameter.view.*

class AssemblyFitting2ProductSub : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ProductSubModel.Data>()
    private var eventHandler: RecyclerClickListener? = null
    private var selectedID = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_diameter, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        if (selectedID == vendorList[position].idSubProduk) {
            holder.itemView.root_lay.setBackgroundResource(R.color.trans_background)
            holder.itemView.tx_name.setTextColor(Color.parseColor("#ffffff"))
            holder.itemView.tx_category.setTextColor(Color.parseColor("#ffffff"))
        } else {
            holder.itemView.root_lay.setBackgroundResource(R.color.white)
            holder.itemView.tx_name.setTextColor(Color.parseColor("#000000"))
            holder.itemView.tx_category.setTextColor(Color.parseColor("#000000"))
        }

        holder.itemView.setOnClickListener { view ->
            selectedID = vendorList[position].idSubProduk
            eventHandler?.isClicked(vendorList[position])
            notifyDataSetChanged()
        }

    }

    fun setList(listOfVendor: List<ProductSubModel.Data>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ProductSubModel.Data) {

            itemView.tx_name.text = vendorModel.size


        }
    }


    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(data: ProductSubModel.Data)
    }

}