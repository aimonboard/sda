package com.sda.indonesia.ui.adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.BrandModel
import kotlinx.android.synthetic.main.item_filter.view.*

class FilterTypeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<String>()
    private var eventHandler: RecyclerClickListener? = null
    private val itemStateArray = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_filter, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        val dash = vendorList[position]

        holder.itemView.setOnClickListener { view ->

            notifyDataSetChanged()
            itemStateArray.clear()

            if (!itemStateArray.contains(dash)) {
                itemStateArray.add(dash)
                eventHandler?.isClicked(dash)
            } else {
                itemStateArray.remove(dash)
                holder.itemView.root_lay.setBackgroundResource(R.drawable.text_shape)
                holder.itemView.tx_category.setTextColor(Color.parseColor("#000000"))
            }
        }

        if (itemStateArray.contains(dash)) {
            holder.itemView.root_lay.setBackgroundResource(R.drawable.text_shape_yes)
            holder.itemView.tx_category.setTextColor(Color.parseColor("#ffffff"))
        } else {
            holder.itemView.root_lay.setBackgroundResource(R.drawable.text_shape)
            holder.itemView.tx_category.setTextColor(Color.parseColor("#000000"))
        }

    }

    fun setList(restoreId: String, listOfVendor: List<String>) {
        this.vendorList = listOfVendor.toMutableList()
        itemStateArray.clear()
        notifyDataSetChanged()

        if (restoreId.isNotEmpty()) {
            itemStateArray.add(restoreId)
            Log.d("aim","category set list : $restoreId")
        }
    }

//    fun restoreState(dash: String) {
//        itemStateArray.clear()
//        itemStateArray.add(dash)
//        notifyDataSetChanged()
//    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: String) {
            itemView.tx_category.text = vendorModel
        }
    }


    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(name: String)
    }

}