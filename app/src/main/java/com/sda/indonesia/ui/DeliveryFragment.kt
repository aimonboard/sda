package com.sda.indonesia.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior

import com.sda.indonesia.R
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.data.local.database.cart.CartEntity
import com.sda.indonesia.data.local.database.cart.CartViewModelDB
import com.sda.indonesia.data.model.sda.DeliveryModel
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.adapter.DeliveryAdapter
import com.sda.indonesia.ui.adapter.ShippingCostAdapter
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.DeliveryViewModel
import kotlinx.android.synthetic.main.bottom_sheet_shipping.*
import kotlinx.android.synthetic.main.delivery_fragment.*
import kotlinx.android.synthetic.main.sub_header.*



class DeliveryFragment : Fragment() {

    private lateinit var shippingBottomSheet : BottomSheetBehavior<ConstraintLayout>
    private lateinit var viewModel: DeliveryViewModel
    private lateinit var cartDB: CartViewModelDB

    private val adapter = DeliveryAdapter()
    private var arrayID : List<String>? = null
    private var arrayQty = ArrayList<Int>()
    private var totalWeight = 0.0F
    private var totalPrice = 0.0
    private var grandTotal = 0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.delivery_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this@DeliveryFragment).get(DeliveryViewModel::class.java)
        cartDB = ViewModelProviders.of(this@DeliveryFragment).get(CartViewModelDB::class.java)

        header_name?.text = "Shipping"
        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        getBundleData()

        generateView()


        setupAdapter()
        getCartOffline()

        btnListener()

        shippingBottomSheet = BottomSheetBehavior.from(option_lay)
        shippingBottomSheet.state = BottomSheetBehavior.STATE_HIDDEN

        btn_buy?.setOnClickListener {
//            if (totalPrice > 0.0) {
//                Navigation.findNavController(it).navigate(R.id.deliveryFragment)
//            } else {
//                FunHelper.snackBar(root_lay,"Choose at least one product")
//            }
            val bundle = Bundle()
            bundle.putFloat("total",grandTotal.toFloat())
            Navigation.findNavController(it).navigate(R.id.payment,bundle)
        }



    }

    private fun generateView() {
        val address = SharedPreferenceHelper.getSharedPreferenceString(activity,111,"")
        val post    = SharedPreferenceHelper.getSharedPreferenceString(activity,222,"")
        val city    = SharedPreferenceHelper.getSharedPreferenceString(activity,333,"")
        val cityID  = SharedPreferenceHelper.getSharedPreferenceString(activity,444,"")
        val prov    = SharedPreferenceHelper.getSharedPreferenceString(activity,555,"")
        val fullAddress = "$address, $city - $post, $prov"

        if (!address.isNullOrEmpty()) {
            btn_edit_address?.visibility = View.VISIBLE
            address_lay?.visibility = View.VISIBLE
            btn_set_address?.visibility = View.GONE
            lay2.visibility = View.VISIBLE
            separator4.visibility = View.VISIBLE

            if (!cityID.isNullOrEmpty()) {
                shippingCost()
                setupObservers()
            }

            val phone = SharedPreferenceHelper.getSharedPreferenceString(activity,2222,"")
            name_value?.text = SharedPreferenceHelper.getSharedPreferenceString(activity,1111,"")
            phone_value?.text = "($phone)"
            address_value?.text = fullAddress
        } else {
            btn_edit_address?.visibility = View.INVISIBLE
            address_lay?.visibility = View.GONE
            btn_set_address?.visibility = View.VISIBLE
            lay2.visibility = View.GONE
            separator4.visibility = View.GONE
        }
    }

    private fun getBundleData() {
        val idArray = requireArguments().getString("cartID")?.replace("[","")?.replace("]","")
        arrayID = idArray?.split(",")

        val iniString = requireArguments().getString("cartQty")?.replace("[","")?.replace("]","")
        val qtyString = iniString?.split(",")

        totalWeight = requireArguments().getFloat("totalWeight")

        arrayQty.clear()
        qtyString?.forEach {
            arrayQty.add(it.replace("\\s".toRegex(), "").toInt())
        }

        Log.d("aim", "ID : $arrayID")
        Log.d("aim", "QTY : $arrayQty")
    }

    private fun btnListener() {
        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        btn_set_address?.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("edit",false)
            Navigation.findNavController(it).navigate(R.id.addressFragment,bundle)
        }
        btn_edit_address?.setOnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("edit",true)
            Navigation.findNavController(it).navigate(R.id.addressFragment,bundle)
        }

        btn_option?.setOnClickListener {
            if (shippingBottomSheet.state == BottomSheetBehavior.STATE_EXPANDED) {
                shippingBottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
            } else {
                shippingBottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        btn_close?.setOnClickListener {
            shippingBottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun shippingCost() {
        val cityID = SharedPreferenceHelper.getSharedPreferenceString(activity,444,"")

        if (cityID != null) {
            val params = HashMap<String, String>()
            params["origin"] = "444"
            params["destination"] = cityID
            params["weight"] = "${totalWeight * 1000}"
            params["courier"] = "jne"

            viewModel.getShippingCost(params)
            Log.d("aim","shipping param : $params")
        }
    }

    private fun setupObservers() {
        val adapter = ShippingCostAdapter()
        recycler_shipping?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_shipping?.adapter = adapter

        shipping_loading?.visibility = View.VISIBLE
        viewModel.listCost.observe(viewLifecycleOwner, Observer {
            if (it.rajaongkir.results.isNotEmpty()) {
                shipping_loading?.visibility = View.GONE
                adapter.setList(it.rajaongkir.results[0].costs)

                val name = it.rajaongkir.results[0].costs[0].service
                val estimate = it.rajaongkir.results[0].costs[0].cost[0].etd
                val price = it.rajaongkir.results[0].costs[0].cost[0].value

                shipping_name?.text = "$name ($estimate Day)"
                shipping_price?.text = FunHelper.bank(price)

                grandTotal = totalPrice + price
                total_value?.text = FunHelper.bank(totalPrice + price)

            } else {
                shipping_loading?.visibility = View.GONE
            }
        })


        adapter.setEventHandler(object: ShippingCostAdapter.RecyclerClickListener {
            override fun isClicked(name: String, price: Double) {
                shipping_name?.text = name
                shipping_price?.text = FunHelper.bank(price)
                shippingBottomSheet.state = BottomSheetBehavior.STATE_HIDDEN

                shipping_price?.text = FunHelper.bank(price)
                total_value?.text = FunHelper.bank(totalPrice + price)
            }
        })
    }

    private fun getCartOffline() {
        cartDB.getAll().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                val mFilter = mutableListOf<DeliveryModel>()
                if (!arrayID.isNullOrEmpty()) {

                    arrayID?.forEachIndexed { indexID, id ->

                        Log.d("aim", "foreach id : $id")
                        val test = it.filter { temp -> temp.sub_id == id.replace("\\s".toRegex(), "") }
                        test.forEachIndexed { index, data ->

                            val mDeliveryModel = DeliveryModel(data.product_name,arrayQty[indexID],data.sub_id,data.sub_price * arrayQty[indexID])
                            if (!mFilter.contains(mDeliveryModel)) {
                                mFilter.add(mDeliveryModel)
                            }
                        }

                    }

                    totalPrice = mFilter.sumByDouble { price -> price.subPrice}


                }
                adapter.setList(mFilter)
            }
        })


    }

    private fun setupAdapter() {
        recycler_item?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_item?.adapter = adapter
    }

}
