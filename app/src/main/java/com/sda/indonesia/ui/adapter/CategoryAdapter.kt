package com.sda.indonesia.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.CategoryModel
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<CategoryModel.Data>()
    private var eventHandler: RecyclerClickListener? = null
    private val itemStateArray = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        val idCategory = vendorList[position].idKategori

        holder.itemView.setOnClickListener { view ->

            notifyDataSetChanged()
            itemStateArray.clear()

            if (!itemStateArray.contains(idCategory)) {
                itemStateArray.add(idCategory)
                val id = vendorList[position].idKategori
                val name = vendorList[position].namaKategori
                eventHandler?.isClicked(id,name)
            } else {
                itemStateArray.remove(idCategory)
            }
        }

        if (itemStateArray.contains(idCategory)) {
            holder.itemView.root_lay.setBackgroundResource(android.R.color.white)
        } else {
            holder.itemView.root_lay.setBackgroundResource(R.color.background_main)
        }

    }

    fun setList(categorySelected: String, listOfVendor: List<CategoryModel.Data>) {
        this.vendorList = listOfVendor.toMutableList()
        itemStateArray.clear()
        notifyDataSetChanged()

        val filter = vendorList.filter { it.idKategori == categorySelected }
        itemStateArray.add(filter[0].idKategori)
        eventHandler?.isClicked(filter[0].idKategori, filter[0].namaKategori)
    }

    fun fakeClick(id: String, name: String) {
        itemStateArray.add(id)
        eventHandler?.isClicked(id, name)
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: CategoryModel.Data) {

            try { Glide.with(itemView).load(vendorModel.imgIcon).into(itemView.image) }
            catch (e: Exception) { }
            itemView.tx_category.text = vendorModel.namaKategori
        }
    }


    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(id: String, name: String)
    }

}