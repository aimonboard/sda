package com.sda.indonesia.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.ScrollView


class CustomScrollView(context: Context?, attrs: AttributeSet?) :
    ScrollView(context, attrs) {
    private lateinit var scrollerTask: Runnable
    private var initialPosition = 0
    private val newCheck = 100

    interface OnScrollStoppedListener {
        fun onScrollStopped()
    }

    private var onScrollStoppedListener: OnScrollStoppedListener? = null
    fun setOnScrollStoppedListener(listener: OnScrollStoppedListener?) {
        onScrollStoppedListener = listener
    }

    fun startScrollerTask() {
        initialPosition = scrollY
        postDelayed(scrollerTask, newCheck.toLong())
    }

    companion object {
        private const val TAG = "MyScrollView"
    }

    init {
        scrollerTask = Runnable {
            val newPosition = scrollY
            if (initialPosition - newPosition == 0) { //has stopped
                if (onScrollStoppedListener != null) {
                    onScrollStoppedListener!!.onScrollStopped()
                }
            } else {
                initialPosition = scrollY
                postDelayed(scrollerTask, newCheck.toLong())
            }
        }
    }
}