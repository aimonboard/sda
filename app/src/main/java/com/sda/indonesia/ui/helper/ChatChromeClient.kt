//package com.sda.indonesia.ui.helper
//
//import android.annotation.TargetApi
//import android.content.ActivityNotFoundException
//import android.content.Intent
//import android.net.Uri
//import android.os.Build
//import android.webkit.ValueCallback
//import android.webkit.WebChromeClient
//import android.webkit.WebView
//import android.widget.Toast
//import androidx.core.app.ActivityCompat.startActivityForResult
//
//
//class ChatChromeClient : WebChromeClient() {
//
//    // For Lollipop 5.0+ Devices
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    override fun onShowFileChooser(mWebView: WebView?, filePathCallback: ValueCallback<Array<Uri?>?>, fileChooserParams: FileChooserParams ): Boolean {
//        if (uploadMessage != null) {
//            uploadMessage.onReceiveValue(null)
//            uploadMessage = null
//        }
//        uploadMessage = filePathCallback
//        val intent = fileChooserParams.createIntent()
//        try {
//            startActivityForResult(intent, REQUEST_SELECT_FILE)
//        } catch (e: ActivityNotFoundException) {
//            uploadMessage = null
//            Toast.makeText(this@WebLink, "Cannot Open File Chooser", Toast.LENGTH_LONG).show()
//            return false
//        }
//        return true
//    }
//
//    private fun openFileChooser(uploadMsg: ValueCallback<Uri?>) {
//        mUploadMessage = uploadMsg
//        val i = Intent(Intent.ACTION_GET_CONTENT)
//        i.addCategory(Intent.CATEGORY_OPENABLE)
//        i.type = "image/*"
//        startActivityForResult(
//            Intent.createChooser(i, "File Chooser"),
//            FILECHOOSER_RESULTCODE
//        )
//    }
//}