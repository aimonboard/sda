package com.sda.indonesia.ui

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

import com.sda.indonesia.R
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.databinding.ListFragmentBinding
import com.sda.indonesia.ui.adapter.FilterBrandAdapter
import com.sda.indonesia.ui.adapter.FilterPressureAdapter
import com.sda.indonesia.ui.adapter.FilterTypeAdapter
import com.sda.indonesia.ui.adapter.ListProductAdapter
import com.sda.indonesia.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.list_fragment.*

class ListFragment : Fragment() {

    private lateinit var viewDataBinding: ListFragmentBinding
    private val adapter = ListProductAdapter()

    // Filter
    private var hasFilter = false
    private var subProductFilter = mutableListOf<ProductModel.Data>()
    private val typeModel = mutableListOf<String>()
    private val typeAdapter = FilterTypeAdapter()
    private var typeValue = ""

    private val brandModel = mutableListOf<String>()
    private val brandAdapter = FilterBrandAdapter()
    private var brandValue = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        viewDataBinding = ListFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = ViewModelProviders.of(this@ListFragment).get(ListViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        generateView()
        btnListener()

        setupAdapter()
        setupObservers()
    }

    override fun onResume() {
        super.onResume()
        Log.d("aim", "OnResume : $hasFilter")
        Log.d("aim", "OnResume : ${subProductFilter.size}")
        if (hasFilter) {
//            adapter.setList(subProductFilter)
            adapter.notifyDataSetChanged()
        }

    }

    private fun btnListener() {
        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        swipe_container?.setOnRefreshListener {
            setupAdapter()
            setupObservers()
        }

        btn_filter?.setOnClickListener {
//            Navigation.findNavController(it).navigate(R.id.searchFragment)
            dialogFilter()
        }
    }

    private fun generateView() {
        val headerName = requireArguments().getString("title")
        header_name?.text = if (!headerName.isNullOrEmpty()) { headerName } else { "Product" }

        val idCategorySub = requireArguments().getString("idCategorySub")
        val search = requireArguments().getString("search")

        viewDataBinding.viewmodel?.getListProduct(
            idCategorySub,
            search
        )

    }

    private fun setupObservers() {
        swipe_container?.isRefreshing = true
        viewDataBinding.viewmodel?.listProductData?.observe(viewLifecycleOwner, Observer {data ->
            if (data.data.isNotEmpty()) {
                swipe_container?.isRefreshing = false
                adapter.setList(data.data)

                subProductFilter.clear()
                typeModel.clear()
                brandModel.clear()

                subProductFilter = data.data.toMutableList()
                for (element in data.data) {
                    val filterType = typeModel.find { it == element.produkType } ?: ""
                    if (filterType.isEmpty()) {
                        typeModel.add(element.produkType)
                    }

                    val filterBrand = brandModel.find { it == element.namaBrand } ?: ""
                    if (filterBrand.isEmpty()) {
                        brandModel.add(element.namaBrand)
                    }
                }

            } else {
                swipe_container?.isRefreshing = false
                adapter.setList(emptyList())
                Toast.makeText(activity,"Empty Data",Toast.LENGTH_LONG ).show()
            }
        })
    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.viewmodel
        if (viewModel != null) {
            recycler_list_product?.layoutManager = GridLayoutManager(activity,2)
            recycler_list_product?.adapter = adapter
        }
    }

    private fun dialogFilter() {

        listenerType()
        listenerBrand()

        val view = layoutInflater.inflate(R.layout.dialog_filter,null)
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(view)

        val bottomSheetBehavior = BottomSheetBehavior.from<View>(view.parent as View)
        dialog.setOnShowListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

        val txType = dialog.findViewById<TextView>(R.id.tx_diameter)
        val txBrand = dialog.findViewById<TextView>(R.id.tx_pressure)
        val recyclerSize = dialog.findViewById<RecyclerView>(R.id.recycler_size)
        val recyclerBrand = dialog.findViewById<RecyclerView>(R.id.recycler_pressure)
        val btnApply = dialog.findViewById<TextView>(R.id.btn_apply)
        val btnReset = dialog.findViewById<TextView>(R.id.btn_reset)

        txType?.text = "Type"
        txBrand?.text = "Brand"

        val typeFlex = FlexboxLayoutManager(context)
        typeFlex.flexDirection = FlexDirection.ROW
        typeFlex.flexWrap = FlexWrap.WRAP
        recyclerSize?.layoutManager = typeFlex
        recyclerSize?.adapter = typeAdapter
        typeAdapter.setList(typeValue, typeModel)

        val brandFlex = FlexboxLayoutManager(context)
        brandFlex.flexDirection = FlexDirection.ROW
        brandFlex.flexWrap = FlexWrap.WRAP
        recyclerBrand?.layoutManager = brandFlex
        recyclerBrand?.adapter = brandAdapter
        brandAdapter.setList(brandValue, brandModel)

        btnApply?.setOnClickListener {
            hasFilter = true
            val filterResult = subProductFilter.filter {
                it.produkType.contains(typeValue,true) && it.namaBrand.contains(brandValue,true)
            }
            adapter.setList(filterResult)

            if (filterResult.isEmpty()) {
                tx_filter.visibility = View.VISIBLE
            } else {
                tx_filter.visibility = View.GONE
            }

            dialog.dismiss()
        }

        btnReset?.setOnClickListener {
            hasFilter = false
            tx_filter.visibility = View.GONE
            typeValue = ""
            brandValue = ""

            adapter.setList(subProductFilter)
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun listenerType() {
        typeAdapter.setEventHandler(object: FilterTypeAdapter.RecyclerClickListener {
            override fun isClicked(name: String) {
                typeValue = name
            }
        })
    }

    private fun listenerBrand() {
        brandAdapter.setEventHandler(object: FilterBrandAdapter.RecyclerClickListener {
            override fun isClicked(name: String) {
                brandValue = name
            }
        })
    }

}
