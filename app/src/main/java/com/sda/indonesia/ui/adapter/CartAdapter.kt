package com.sda.indonesia.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.local.database.cart.CartEntity
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_cart.view.*

class CartAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<CartEntity>()
    private var eventHandler: RecyclerClickListener? = null

    private val itemStateArray = ArrayList<String>()
    private val priceArray = ArrayList<Double>()
    private val qtyArray = ArrayList<Int>()
    private val weightArray = ArrayList<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        val mData = vendorList[position]

        holder.itemView.btn_delete.setOnClickListener {
            eventHandler?.isDelete(position, mData)
        }

        holder.itemView.btn_plus.setOnClickListener {
            val qty = holder.itemView.qty_value.text.toString().toInt()
            val qtyFinal = qty + 1

            if (qtyFinal <= mData.sub_stock ) {
                holder.itemView.qty_value.text = qtyFinal.toString()
                holder.itemView.note_value?.text = ""

                if (itemStateArray.contains(mData.sub_id)) {
                    val aIndex = itemStateArray.indexOf(mData.sub_id)
                    itemStateArray[aIndex] = mData.sub_id
                    qtyArray[aIndex] = qtyFinal
                    priceArray[aIndex] = qtyArray[aIndex] * mData.sub_price
                    weightArray[aIndex] = qtyArray[aIndex] * mData.sub_weight
                }

                eventHandler?.isClicked(itemStateArray, qtyArray, priceArray, weightArray)

            }
            else {
                holder.itemView.note_value?.text = "Max ${mData.sub_stock} item"
            }
        }

        holder.itemView.btn_minus.setOnClickListener {
            val qty = holder.itemView.qty_value.text.toString().toInt()
            val qtyFinal = qty - 1

            if (qtyFinal > 0) {
                holder.itemView.qty_value.text = qtyFinal.toString()
                holder.itemView.note_value?.text = ""

                if (itemStateArray.contains(mData.sub_id)) {
                    val aIndex = itemStateArray.indexOf(mData.sub_id)
                    itemStateArray[aIndex] = mData.sub_id
                    qtyArray[aIndex] = qtyFinal
                    priceArray[aIndex] = qtyArray[aIndex] * mData.sub_price
                    weightArray[aIndex] = qtyArray[aIndex] * mData.sub_weight
                }

                eventHandler?.isClicked(itemStateArray, qtyArray, priceArray, weightArray)

            }
        }

        holder.itemView.checkBox_product.setOnCheckedChangeListener { buttonView, isChecked ->
            val qty = holder.itemView.qty_value.text.toString().toInt()

            if (isChecked) {
                if (!itemStateArray.contains(mData.sub_id)) {
                    itemStateArray.add(mData.sub_id)

                    qtyArray.add(qty)
                    priceArray.add(qty * mData.sub_price)
                    weightArray.add(qty * mData.sub_weight)
                }
            } else {
                if (itemStateArray.contains(mData.sub_id)) {
                    val aIndex = itemStateArray.indexOf(mData.sub_id)
                    itemStateArray.removeAt(aIndex)

                    qtyArray.removeAt(aIndex)
                    priceArray.removeAt(aIndex)
                    weightArray.removeAt(aIndex)
                }
            }

            eventHandler?.isClicked(itemStateArray, qtyArray, priceArray, weightArray)
        }

        holder.itemView.checkBox_product.isChecked = itemStateArray.contains(mData.sub_id)

        if (position == 0) {
            holder.itemView.rv_header.visibility = View.VISIBLE
//            holder.itemView.separator2.visibility = View.GONE
        } else {

            if (vendorList[position].product_id == vendorList[position - 1].product_id) {
                holder.itemView.rv_header.visibility = View.GONE
            } else {
                holder.itemView.rv_header.visibility = View.VISIBLE
            }

            if (position == itemCount-1) {
                holder.itemView.separator2.visibility = View.GONE
            }

            if (position <= itemCount-2) {
                if (vendorList[position].product_id != vendorList[position + 1].product_id) {
                    holder.itemView.separator2.visibility = View.GONE
                } else {
                    holder.itemView.separator2.visibility = View.VISIBLE
                }
            }

        }
    }

    fun setList(listOfVendor: List<CartEntity>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: CartEntity) {

            if (vendorModel.child != null) {
                // Hose Assembly - Custom flow
                itemView.recycler_sub.layoutManager = LinearLayoutManager(itemView.context, RecyclerView.VERTICAL, false)
                val childAdapter = CartSubAdapter()
                itemView.recycler_sub.adapter = childAdapter
                childAdapter.setList(vendorModel.child)

                itemView.rv_header_name.text = vendorModel.product_name
                itemView.rv_header_category.text = vendorModel.product_category
                itemView.price_value.text = FunHelper.bank(vendorModel.sub_price)
                try { Glide.with(itemView).load(vendorModel.product_img).into(itemView.rv_header_img) } catch (e: Exception) { }

                itemView.main_cart_lay.visibility = View.GONE
                itemView.recycler_sub.visibility = View.VISIBLE
            } else {
                // Normal flow
                itemView.rv_header_name.text = vendorModel.product_name
                itemView.rv_header_category.text = vendorModel.product_category
                itemView.price_value.text = FunHelper.bank(vendorModel.sub_price)
                try { Glide.with(itemView).load(vendorModel.product_img).into(itemView.rv_header_img) } catch (e: Exception) { }

                itemView.main_cart_lay.visibility = View.VISIBLE
                itemView.recycler_sub.visibility = View.GONE

                var dash = vendorModel.sub_dash
                dash += vendorModel.sub_dash_uom
                itemView.dash_value.text = dash.ifBlank { "-" }

                var diameter = vendorModel.sub_height
                diameter += " ${vendorModel.sub_height_uom}"
                itemView.diameter_value.text = diameter.ifBlank { "-" }

                var length = vendorModel.sub_length
                length += " ${vendorModel.sub_length_uom}"
                itemView.length_value.text = length.ifBlank { "-" }

                var pressure = vendorModel.sub_pressure
                pressure += " ${vendorModel.sub_pressure_uom}"
                itemView.pres_value.text = pressure.ifBlank { "-" }

                var weight: String = vendorModel.sub_weight.toString()
                weight += " ${vendorModel.sub_weight_uom}"
                itemView.weight_value.text = weight.ifBlank { "-" }
            }

        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(itemID: ArrayList<String>, itemQty: ArrayList<Int>, itemPrice: ArrayList<Double>, itemWeight: ArrayList<Int> )
        fun isDelete(position: Int, data: CartEntity)
    }

}