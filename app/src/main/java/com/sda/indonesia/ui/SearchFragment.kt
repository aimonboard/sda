package com.sda.indonesia.ui


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sda.indonesia.BR.viewmodel

import com.sda.indonesia.R
import com.sda.indonesia.databinding.FragmentSearchBinding
import com.sda.indonesia.ui.adapter.SearchCategoryAdapter
import com.sda.indonesia.ui.adapter.SearchCategorySubAdapter
import kotlinx.android.synthetic.main.fragment_search.*
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.flexbox.*
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.ui.adapter.SearchBrandAdapter
import com.sda.indonesia.viewmodel.SearchViewModel
import kotlinx.android.synthetic.main.sub_header.*


class SearchFragment : Fragment() {

    private lateinit var viewDataBinding: FragmentSearchBinding
    private val categoryAdapter = SearchCategoryAdapter()
    private val categorySubAdapter = SearchCategorySubAdapter()
    private val brandAdapter = SearchBrandAdapter()

    private var idCategory = ""
    private var idCategorySub = ""
    private var idBrand = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        viewDataBinding = FragmentSearchBinding.inflate(inflater, container, false).apply {
            viewmodel = ViewModelProviders.of(this@SearchFragment).get(SearchViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        header_name?.text = "Filter"

        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        viewDataBinding.viewmodel?.getCategory()
        viewDataBinding.viewmodel?.getBrand()

        generateView()
        generateListener()

        listenerCategory()
        listenerCategorySub()
        listenerBrand()

        setupAdapter()
        setupObservers()
    }

    private fun generateView() {
        val restoreIdCategory = SharedPreferenceHelper.getSharedPreferenceString(activity,22,"")
        if (!restoreIdCategory.isNullOrEmpty()) {
            idCategory = restoreIdCategory
        }

        val restoreIdCategorySub = SharedPreferenceHelper.getSharedPreferenceString(activity,33,"")
        if (!restoreIdCategorySub.isNullOrEmpty()) {
            idCategorySub = restoreIdCategorySub
        }

        val restoreIdBrand = SharedPreferenceHelper.getSharedPreferenceString(activity,44,"")
        if (!restoreIdBrand.isNullOrEmpty()) {
            idBrand = restoreIdBrand
        }
    }

    private fun generateListener() {

        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        swipe_container?.setOnRefreshListener {
            setupAdapter()
            setupObservers()
        }

        btn_apply?.setOnClickListener {
            Log.d("aim","btn apply click")
            SharedPreferenceHelper.setSharedPreferenceString(context,11,"1")
            SharedPreferenceHelper.setSharedPreferenceString(context,22,idCategory)
            SharedPreferenceHelper.setSharedPreferenceString(context,33,idCategorySub)
            SharedPreferenceHelper.setSharedPreferenceString(context,44,idBrand)

//            Navigation.findNavController(it).previousBackStackEntry?.savedStateHandle?.set("key", "")
            Navigation.findNavController(it).popBackStack()

        }

        btn_reset?.setOnClickListener {
            SharedPreferenceHelper.setSharedPreferenceString(context,11,"0")
            SharedPreferenceHelper.setSharedPreferenceString(context,22,"")
            SharedPreferenceHelper.setSharedPreferenceString(context,33,"")
            SharedPreferenceHelper.setSharedPreferenceString(context,44,"")

//            Navigation.findNavController(it).previousBackStackEntry?.savedStateHandle?.set("key", "")
            Navigation.findNavController(it).popBackStack()
        }

    }


    private fun setupObservers() {
        swipe_container?.isRefreshing = true
        viewDataBinding.viewmodel?.categoryData?.observe(viewLifecycleOwner, Observer {
            if (it.data.isNotEmpty()) {
                swipe_container?.isRefreshing = false
                categoryAdapter.setList(idCategory, it.data)
            } else {
                swipe_container?.isRefreshing = false
            }
        })

        viewDataBinding.viewmodel?.categorySubData?.observe(viewLifecycleOwner, Observer {
            if (it.data.isNotEmpty()) {
                swipe_container?.isRefreshing = false
                categorySubAdapter.setList(idCategorySub, it.data)
            } else {
                swipe_container?.isRefreshing = false
            }
        })

        viewDataBinding.viewmodel?.brandData?.observe(viewLifecycleOwner, Observer {
            if (it.data.isNotEmpty()) {
                swipe_container?.isRefreshing = false
                brandAdapter.setList(idBrand,it.data)
            } else {
                swipe_container?.isRefreshing = false
            }
        })
    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.viewmodel
        if (viewModel != null) {
            val gridCategory = FlexboxLayoutManager(context)
            gridCategory.flexDirection = FlexDirection.ROW
            gridCategory.flexWrap = FlexWrap.WRAP
            recycler_category?.layoutManager = gridCategory
            recycler_category?.adapter = categoryAdapter

            val gridCategorySub = FlexboxLayoutManager(context)
            gridCategorySub.flexDirection = FlexDirection.ROW
            gridCategorySub.flexWrap = FlexWrap.WRAP
            recycler_sub_category?.layoutManager = gridCategorySub
            recycler_sub_category?.adapter = categorySubAdapter

            val gridBrand = FlexboxLayoutManager(context)
            gridBrand.flexDirection = FlexDirection.ROW
            gridBrand.flexWrap = FlexWrap.WRAP
            recycler_brand?.layoutManager = gridBrand
            recycler_brand?.adapter = brandAdapter
        }
    }

    private fun listenerCategory() {
        categoryAdapter.setEventHandler(object: SearchCategoryAdapter.RecyclerClickListener {
            override fun isClicked(id: String, name: String) {
                Log.d("aim","category clicked $id")
                category_sub_lay?.visibility = View.VISIBLE
//                viewDataBinding.viewmodel?.getCategorySub(id)
                idCategory = id
            }
        })
    }

    private fun listenerCategorySub() {
        categorySubAdapter.setEventHandler(object: SearchCategorySubAdapter.RecyclerClickListener {
            override fun isClicked(id: String) {
                Log.d("aim","Sub category clicked $id")
                idCategorySub = id
            }

        })
    }

    private  fun listenerBrand() {
        brandAdapter.setEventHandler(object: SearchBrandAdapter.RecyclerClickListener {
            override fun isClicked(id: String, name: String) {
                Log.d("aim","brand clicked $id")
                idBrand = id
            }

        })
    }


}
