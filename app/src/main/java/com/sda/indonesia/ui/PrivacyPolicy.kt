package com.sda.indonesia.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import com.sda.indonesia.R
import kotlinx.android.synthetic.main.fragment_privacy_policy.*

class PrivacyPolicy : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val url = requireArguments().getString("url") ?: ""

        if (url.isNotEmpty()) {
            val mWebSettings = web_view.settings
            mWebSettings.javaScriptEnabled = true
            web_view.settings.domStorageEnabled = true
            mWebSettings.allowContentAccess = true
            mWebSettings.allowFileAccess = true
            web_view.loadUrl(url)

            web_view.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    loading?.visibility = View.GONE
                }
            }
        }

    }
}