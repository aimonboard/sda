package com.sda.indonesia.ui.adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.CategoryModel
import kotlinx.android.synthetic.main.item_search.view.*

class SearchCategoryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<CategoryModel.Data>()
    private var eventHandler: RecyclerClickListener? = null
    private val itemStateArray = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        val idCategory = vendorList[position].idKategori

        holder.itemView.setOnClickListener { view ->

            notifyDataSetChanged()
            itemStateArray.clear()

            if (!itemStateArray.contains(idCategory)) {
                itemStateArray.add(idCategory)
                val id = vendorList[position].idKategori
                val name = vendorList[position].namaKategori
                eventHandler?.isClicked(id,name)
            } else {
                itemStateArray.remove(idCategory)
                holder.itemView.root_lay.setBackgroundResource(R.drawable.text_shape)
                holder.itemView.tx_category.setTextColor(Color.parseColor("#000000"))
            }
        }

        if (itemStateArray.contains(idCategory)) {
            holder.itemView.root_lay.setBackgroundResource(R.drawable.text_shape_yes)
            holder.itemView.tx_category.setTextColor(Color.parseColor("#ffffff"))
        } else {
            holder.itemView.root_lay.setBackgroundResource(R.drawable.text_shape)
            holder.itemView.tx_category.setTextColor(Color.parseColor("#000000"))
        }

    }

    fun setList(restoreId: String, listOfVendor: List<CategoryModel.Data>) {
        this.vendorList = listOfVendor.toMutableList()
        itemStateArray.clear()
        notifyDataSetChanged()

        if (restoreId.isNotEmpty()) {
            itemStateArray.add(restoreId)
            Log.d("aim","category set list : $restoreId")
        }
    }

//    fun restoreState(idCategory: String) {
//        itemStateArray.clear()
//        itemStateArray.add(idCategory)
//        notifyDataSetChanged()
//    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: CategoryModel.Data) {
            itemView.tx_category.text = vendorModel.namaKategori
        }
    }


    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(id: String, name: String)
    }

}