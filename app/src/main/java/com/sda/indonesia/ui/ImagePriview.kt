package com.sda.indonesia.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.sda.indonesia.R
import kotlinx.android.synthetic.main.fragment_image_priview.*
import kotlinx.android.synthetic.main.sub_header.*

class ImagePriview : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image_priview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        header_name?.text = "Product Preview"
        back_link?.setOnClickListener { activity?.onBackPressed() }
        val image = arguments!!.getString("image")
        try { Glide.with(activity!!).load(image).into(image_lay) } catch (e: Exception) {  }

    }


}
