package com.sda.indonesia.ui

import android.app.AlertDialog
import android.app.Dialog
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView

import com.sda.indonesia.R
import com.sda.indonesia.data.local.database.cart.CartEntity
import com.sda.indonesia.data.local.database.cart.CartViewModelDB
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.adapter.CartAdapter
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.CartViewModel
import kotlinx.android.synthetic.main.cart_fragment.*
import android.widget.Toast
import com.sda.indonesia.data.local.SharedPreferenceHelper


class CartFragment : Fragment() {

    private lateinit var viewModel: CartViewModel
    private lateinit var cartDB: CartViewModelDB
    private val adapter = CartAdapter()

    private var cartID = ArrayList<String>()
    private var cartQty = ArrayList<Int>()
    private var cartPrice = ArrayList<Double>()
    private var cartWeight = ArrayList<Int>()

    private var totalPrice = 0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.cart_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this@CartFragment).get(CartViewModel::class.java)
        cartDB = ViewModelProviders.of(this@CartFragment).get(CartViewModelDB::class.java)

        viewModel.getProdukSub("1")

        generateView()
        btnListener()

        setupAdapter()
//        setupObservers()
        getCartOffline()

        listenerCart()

    }

    private fun generateView() {
        val from = requireArguments().getString("from")
        if (from.isNullOrEmpty()) {
            sub_header?.visibility = View.GONE
            val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
            navBar?.visibility = View.VISIBLE
        } else {
            sub_header?.visibility = View.VISIBLE
            val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
            navBar?.visibility = View.GONE
        }
    }

    private fun btnListener() {
        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        btn_buy?.setOnClickListener {
            val loginStatus = SharedPreferenceHelper.getSharedPreferenceString(activity,1,"")
            if (loginStatus == "1") {
                if (totalPrice > 0.0) {

                    if (cartWeight.sum() <= 30) {
                        val bundle = Bundle()
                        bundle.putString("cartID",cartID.toString())
                        bundle.putString("cartQty",cartQty.toString())
                        bundle.putFloat("totalWeight",cartWeight.sum().toFloat())
                        Navigation.findNavController(it).navigate(R.id.deliveryFragment,bundle)
                    } else {
                        FunHelper.snackBar(root_lay,"Order weight ${cartWeight.sum()}kg, limit 30kg)")
                    }

                } else {
                    FunHelper.snackBar(root_lay,"Choose at least one product")
                }
            } else {
                val bundle = Bundle()
                bundle.putString("goto","cart")
                Navigation.findNavController(it).navigate(R.id.loginFragment,bundle)
            }
        }
    }

//    private fun setupObservers() {
//        swipe_container?.isRefreshing = true
//        viewModel.productSubData?.observe(viewLifecycleOwner, Observer {
//            if (it.status) {
//                swipe_container?.isRefreshing = false
//                adapter.setList(it.data)
//            } else {
//                swipe_container?.isRefreshing = false
//                adapter.setList(emptyList())
//            }
//        })
//    }

    private fun getCartOffline() {
        cartDB.getAll().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                val test = it.sortedBy { data -> data.product_id }
                val data = mutableListOf<CartEntity>()

                test.forEach {mData ->
                    data.add(mData)
                }
                adapter.setList(data)
            } else {
                adapter.setList(emptyList())
            }
        })
    }

    private fun setupAdapter() {
        recycler_cart?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_cart?.adapter = adapter
    }

    private fun listenerCart() {
        adapter.setEventHandler(object: CartAdapter.RecyclerClickListener {
            override fun isDelete(position: Int, data: CartEntity) {
                deleteDialog(position, data)
            }

            override fun isClicked(itemID: ArrayList<String>, itemQty: ArrayList<Int>, itemPrice: ArrayList<Double>, itemWeight: ArrayList<Int>) {
                cartID = itemID
                cartQty = itemQty
                cartPrice = itemPrice
                cartWeight = itemWeight

                totalPrice = cartPrice.sum()
                total_value.text = FunHelper.bank(cartPrice.sum())

                Log.d("aim","item state  : $cartID")
                Log.d("aim","item qty    : $cartQty")
                Log.d("aim","item price  : $cartPrice")
                Log.d("aim","item weight : $cartWeight")
            }
        })
    }

    private fun deleteDialog(position: Int, cartEntity: CartEntity) {

        if (cartID.contains(cartEntity.sub_id)) {
            val priceIndex = cartID.indexOf(cartEntity.sub_id)
            cartPrice.removeAt(priceIndex)
        }

        Log.d("aim","Delete ${cartEntity.sub_id}")

        totalPrice = cartPrice.sum()
        total_value.text = FunHelper.bank(cartPrice.sum())

        AlertDialog.Builder(context)
//            .setTitle("Warning")
            .setMessage("Delete this item ?")
//            .setIcon(android.R.drawable.ic_dialog_alert)
            .setPositiveButton(android.R.string.yes) { dialog, whichButton ->
                cartDB.delete(cartEntity)
                adapter.notifyItemRemoved(position)
            }
            .setNegativeButton(android.R.string.no, null).show()
    }

}
