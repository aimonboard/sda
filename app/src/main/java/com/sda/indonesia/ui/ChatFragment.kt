package com.sda.indonesia.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.webkit.WebChromeClient.FileChooserParams
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.sda.indonesia.R
import kotlinx.android.synthetic.main.fragment_chat.*


class ChatFragment : Fragment() {

    private val fileChooserReq = 100

    private var mUploadMessage: ValueCallback<Uri?>? = null
    var uploadMessage: ValueCallback<Array<Uri>?>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mWebSettings = web_view.settings
        mWebSettings.javaScriptEnabled = true
        web_view.settings.domStorageEnabled = true
        mWebSettings.allowContentAccess = true
        mWebSettings.allowFileAccess = true
        web_view.loadUrl("https://tawk.to/chat/5f06f50d5b59f94722ba6803/default")

        web_view.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                loading?.visibility = View.GONE
            }
        }

        web_view.webChromeClient = object : WebChromeClient() {
            // For Lollipop 5.0+ Devices
            override fun onShowFileChooser(mWebView: WebView?,filePathCallback: ValueCallback<Array<Uri>?>?,fileChooserParams: FileChooserParams): Boolean {
                if (uploadMessage != null) {
                    uploadMessage?.onReceiveValue(null)
                    uploadMessage = null
                }

                uploadMessage = filePathCallback
                val intent = fileChooserParams.createIntent()
                try {
                    startActivityForResult(intent, fileChooserReq)
                } catch (e: ActivityNotFoundException) {
                    uploadMessage = null
                    Toast.makeText(requireContext(),"Cannot Open File",Toast.LENGTH_LONG).show()
                    return false
                }
                return true
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (uploadMessage == null) {
            uploadMessage?.onReceiveValue(emptyArray())
            uploadMessage = null
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == fileChooserReq) {
                if (uploadMessage == null) return
                uploadMessage!!.onReceiveValue(
                    FileChooserParams.parseResult(
                        resultCode,
                        intent
                    )
                )
                uploadMessage = null
            }
        } else if (requestCode == fileChooserReq) {
            if (null == mUploadMessage) return
            val result: Uri? =
                if (intent == null || resultCode != Activity.RESULT_OK) null else intent.data
            mUploadMessage!!.onReceiveValue(result)
            mUploadMessage = null
        } else Toast.makeText(requireContext(),"Failed to Upload Image",Toast.LENGTH_LONG).show()
    }
}