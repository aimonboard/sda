package com.sda.indonesia.ui

import android.R.attr
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sda.indonesia.data.model.sda.BulkOrderModel
import com.sda.indonesia.ui.adapter.BulkOrderAdapter
import com.sda.indonesia.viewmodel.BulkOrderViewModel
import kotlinx.android.synthetic.main.bulk_order_fragment.*
import com.sda.indonesia.R
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.bulk_order_fragment.btn_done
import kotlinx.android.synthetic.main.sub_header.*


class BulkOrder : Fragment() {

    private lateinit var viewModel: BulkOrderViewModel
    private val productAdapter = BulkOrderAdapter()

    private val addProductRequest = 1
    private val productData = mutableListOf<BulkOrderModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bulk_order_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BulkOrderViewModel::class.java)
        // TODO: Use the ViewModel
        header_name?.text = "Bulk Order Request"
        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        setupAdapter()

        back_link.setOnClickListener {
            activity?.onBackPressed()
        }

        btn_add.setOnClickListener {

            val intent = Intent(it.context, BulkOrderAdd2::class.java)
            startActivityForResult(intent, addProductRequest)

        }

        btn_done.setOnClickListener {
            if (emptyCheck()) {
                requireActivity().onBackPressed()
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == addProductRequest) {
            if (resultCode == Activity.RESULT_OK) {
                val id = data?.getStringExtra("id")
                val name = data?.getStringExtra("name")
                val type = data?.getStringExtra("type")
                val qty = data?.getStringExtra("qty")
                Log.d("aim", "data diterima: $id, $name")

                if (id != null && name != null && type != null && qty != null) {
                    productAdapter.addData(BulkOrderModel(id, "$name $type", qty))
                    productData.add(BulkOrderModel(id, "$name $type", qty))
                    tx_product.visibility = View.VISIBLE
                    recycler_product.visibility = View.VISIBLE
                }
            }

            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

    }
    private fun emptyCheck(): Boolean{
        var cek = 0

        if(company_value.text.isEmpty()){
            company_lay.error = "Required"; cek++
        } else { company_lay.error = "" }

        if(phone_value.text.isEmpty()){
            phone_lay.error = "Required"; cek++
        } else { phone_lay.error = "" }

        if(address_value.text.isEmpty()){
            address_lay.error = "Required"; cek++
        } else { address_lay.error = "" }

        if(productData.isEmpty()){
            FunHelper.snackBar(requireView(), "Add product to continue this request")
            cek++
        }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun setupAdapter() {
        recycler_product?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_product?.adapter = productAdapter
    }

}
