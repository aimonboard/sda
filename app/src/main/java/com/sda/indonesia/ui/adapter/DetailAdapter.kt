package com.sda.indonesia.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_sub_product.view.*

class DetailAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ProductSubModel.Data>()
    private var tempData = mutableListOf<ProductSubModel.Data>()
    private var eventHandler: RecyclerClickListener? = null

    private var productType = ""

    private val itemStateArray = ArrayList<String>()
    private val priceArray = ArrayList<Double>()
    private var totalPrice = 0.0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sub_product, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(productType, vendorList[position])

        val mData = vendorList[position]

        holder.itemView.checkBox_product.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                if (!itemStateArray.contains(mData.idSubProduk)) {
                    itemStateArray.add(mData.idSubProduk)
                    priceArray.add(mData.hargaProduk)
                    tempData.add(mData)
                }
            } else {
                if (itemStateArray.contains(mData.idSubProduk)) {
                    itemStateArray.remove(mData.idSubProduk)
                    priceArray.remove(mData.hargaProduk)
                    tempData.remove(mData)
                }
            }

            eventHandler?.isClicked(priceArray.sum(), tempData)

        }

        holder.itemView.checkBox_product.isChecked = itemStateArray.contains(mData.idSubProduk)

        Log.d("aim","total price : $totalPrice")

    }

    fun setList(productType: String, listOfVendor: List<ProductSubModel.Data>) {
        this.productType = productType
        this.vendorList = listOfVendor.toMutableList()
        totalPrice = 0.0
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(productType: String, vendorModel: ProductSubModel.Data) {

            itemView.type_value.text = productType

            var size = vendorModel.size ?: ""
            size += " ${vendorModel.namesize ?: ""}"
            itemView.size_value.text = size.ifBlank { "-" }

            var weight: String = vendorModel.weight.toString()
            weight += " ${vendorModel.nameweight}"
            itemView.weight_value.text = weight.ifBlank { "-" }

            itemView.harga_value.text = FunHelper.bank(vendorModel.hargaProduk)


            // visibility by dash & pressure (hydraulic)
            val dash = vendorModel.dash ?: ""
            val pressure = vendorModel.pressure ?: ""

            if (dash.isNotEmpty() && pressure.isNotEmpty()) {
                // is hydraulic
                itemView.dash_value.text = dash.ifBlank { "-" }
                itemView.pres_value.text = pressure.ifBlank { "-" }
            } else {
                // is not hydraulic
                itemView.tx_dash.visibility = View.GONE
                itemView.dash_value.visibility = View.GONE
                itemView.tx_pres.visibility = View.GONE
                itemView.pres_value.visibility = View.GONE
            }
        }
    }


    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(price: Double, data: MutableList<ProductSubModel.Data>)
    }

}