package com.sda.indonesia.ui.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.ProductModel
import kotlinx.android.synthetic.main.item_sub_category.view.*

class ListProductAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ProductModel.Data>()
    private var eventHandler: ListProductAdapter.RecyclerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sub_category, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder

        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener { view ->
            eventHandler?.isClicked(vendorList[position].idProduk, vendorList[position].namaProduk, vendorList[position].produkType)

        }
    }
    fun setList(listOfVendor: List<ProductModel.Data>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun reset() {
        this.vendorList.clear()
        notifyDataSetChanged()
    }


    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ProductModel.Data) {

            itemView.tx_name.text = vendorModel.namaProduk
            itemView.tx_category.text = vendorModel.produkType

            Glide.with(itemView.context)
                .load(vendorModel.imgProduk)
                .into(itemView.photo)
        }
    }

    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(id: String, name: String, type: String)
    }
}