package com.sda.indonesia.ui

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.*
import android.view.inputmethod.InputMethodManager
import com.sda.indonesia.R
import com.sda.indonesia.ui.adapter.HomeAdapter
import kotlinx.android.synthetic.main.home_fragment.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sda.indonesia.data.local.database.category.CategoryViewModelDB
import com.sda.indonesia.data.local.database.categorysub.CategorySubViewModelDB
import com.sda.indonesia.databinding.HomeFragmentBinding
import com.sda.indonesia.viewmodel.HomeViewModel
import com.synnapps.carouselview.ImageListener


class HomeFragment : Fragment() {

    private lateinit var viewDataBinding: HomeFragmentBinding
    private val hydraulicAdapter = HomeAdapter()
    private val fittingAdapter = HomeAdapter()

    private var sampleImages = intArrayOf(
        R.drawable.banner1,
        R.drawable.banner2,
        R.drawable.banner1
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = HomeFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = ViewModelProviders.of(this@HomeFragment).get(HomeViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            scrollContainer.scrollX = savedInstanceState.getInt("scroll")
            recycler_best_seller.scrollX = savedInstanceState.getInt("rvA")
            recycler_promotion.scrollX = savedInstanceState.getInt("rvB")
        }

        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.VISIBLE

        viewDataBinding.viewmodel?.getListHydraulic("1",null)
        viewDataBinding.viewmodel?.getListFitting("10",null)

        swipe_container?.setOnRefreshListener {
            setupAdapter()
            setupObservers()
        }

        val imageListener = ImageListener { position, imageView ->
            imageView?.setImageResource(sampleImages[position])
        }

        carouselView?.setImageListener(imageListener)
        carouselView?.setImageClickListener {
            Log.d("aim","banner click - pos $it")
        }

        carouselView?.pageCount = sampleImages.size

        setupAdapter()
        setupObservers()

        search_text?.setOnEditorActionListener { v, actionId, event ->
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)

            if (search_text.text.isNotEmpty()) {
                val bundle = Bundle()
                bundle.putString("title", "Search Result")
                bundle.putString("search", search_text.text.toString())
                Navigation.findNavController(view).navigate(R.id.listFragment,bundle)
            }
            true
        }

        all_best_seller?.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("title","Hydraulic Hose")
            bundle.putString("idCategorySub","1")
            Navigation.findNavController(it).navigate(R.id.listFragment,bundle)
        }

        all_promotion?.setOnClickListener {
//            val i = Intent(it.context, Register::class.java)
//            startActivity(i)

            val bundle = Bundle()
            bundle.putString("title","Pneumatic Fitting")
            bundle.putString("idCategorySub","10")
            Navigation.findNavController(it).navigate(R.id.listFragment,bundle)
        }

        btn_contact?.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.contactFragment)
        }

        btn_hose?.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.assemblyHoseFragmnet)
        }

        btn_bulk?.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.bulkOrder)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (scrollContainer != null) outState.putInt("scroll", scrollContainer.scrollX)
        if (recycler_best_seller != null) outState.putInt("rvA",recycler_best_seller.scrollX)
        if (recycler_promotion != null) outState.putInt("rvB",recycler_promotion.scrollX)

        Log.d("aim", "on save")
    }

    private fun setupObservers() {
        swipe_container?.isRefreshing = true
        viewDataBinding.viewmodel?.hydraulicData?.observe(viewLifecycleOwner, Observer {
            if (it.data.isNotEmpty()) {
                swipe_container?.isRefreshing = false
                hydraulicAdapter.setList(it.data)
            } else {
                swipe_container?.isRefreshing = false
            }
        })


        viewDataBinding.viewmodel?.fittingData?.observe(viewLifecycleOwner, Observer {
            if (it.data.isNotEmpty()) {
                swipe_container?.isRefreshing = false
                fittingAdapter.setList(it.data)
            } else {
                swipe_container?.isRefreshing = false
            }
        })
    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.viewmodel
        if (viewModel != null) {
            recycler_best_seller?.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
            recycler_best_seller?.adapter = hydraulicAdapter

            recycler_promotion?.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
            recycler_promotion?.adapter = fittingAdapter
        }
    }



}
