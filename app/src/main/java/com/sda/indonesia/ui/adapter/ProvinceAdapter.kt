package com.sda.indonesia.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.rajaongkir.ProvinceModel
import kotlinx.android.synthetic.main.item_text.view.*

class ProvinceAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<ProvinceModel.Rajaongkir.Result>()
    private var eventHandler: RecyclerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_text, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        val idProvince = vendorList[position].provinceId
        val name = vendorList[position].province

        holder.itemView.setOnClickListener { view ->
            eventHandler?.isClicked(idProvince,name)
        }
    }

    fun setList(listOfVendor: List<ProvinceModel.Rajaongkir.Result>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ProvinceModel.Rajaongkir.Result) {
            itemView.name_value.text = vendorModel.province
        }
    }


    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(id: String, name: String)
    }

}