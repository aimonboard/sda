package com.sda.indonesia.ui.adapter

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.ProductModel
import com.sda.indonesia.data.model.sda.TransactionModel
import kotlinx.android.synthetic.main.item_transaction.view.*

class TransactionAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<TransactionModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_transaction, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder

        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener { view ->

        }
    }

    fun setList(listOfVendor: List<TransactionModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun add(data: TransactionModel) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }


    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(data: TransactionModel) {

            when(data.status) {
                "0" -> {
                    itemView.status_value.text = "Cancel"
                    itemView.status_value.setBackgroundResource(R.drawable.shape_solid_red)
                }
                "1" -> {
                    itemView.status_value.text = "Pending"
                    itemView.status_value.setBackgroundResource(R.drawable.shape_solid_orange)
                }
                "2" -> {
                    itemView.status_value.text = "Success"
                    itemView.status_value.setBackgroundResource(R.drawable.shape_solid_green)
                }
            }
        }
    }
}