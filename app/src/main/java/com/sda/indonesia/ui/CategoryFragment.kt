package com.sda.indonesia.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView

import com.sda.indonesia.R
import com.sda.indonesia.data.local.database.category.CategoryViewModelDB
import com.sda.indonesia.data.local.database.categorysub.CategorySubViewModelDB
import com.sda.indonesia.data.model.sda.CategoryModel
import com.sda.indonesia.data.model.sda.CategorySubModel
import com.sda.indonesia.databinding.CategoryFragmentBinding
import com.sda.indonesia.ui.adapter.CategoryAdapter
import com.sda.indonesia.ui.adapter.CategorySubAdapter
import com.sda.indonesia.viewmodel.CategoryViewModel
import kotlinx.android.synthetic.main.category_fragment.*

class CategoryFragment : Fragment() {

    private lateinit var categoryDB: CategoryViewModelDB
    private lateinit var categorySubDB: CategorySubViewModelDB
    private lateinit var viewDataBinding: CategoryFragmentBinding

    private val categoryAdapter = CategoryAdapter()
    private val categorySubAdapter = CategorySubAdapter()

    private var categorySelected = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = CategoryFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = ViewModelProviders.of(this@CategoryFragment).get(CategoryViewModel::class.java)
            categoryDB = ViewModelProviders.of(this@CategoryFragment).get(CategoryViewModelDB::class.java)
            categorySubDB = ViewModelProviders.of(this@CategoryFragment).get(CategorySubViewModelDB::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            categorySelected = savedInstanceState.getString("selectedA")!!
            getCategorySub(categorySelected)

            recycler_category.scrollX = savedInstanceState.getInt("rvA")
            recycler_sub_category.scrollX = savedInstanceState.getInt("rvB")
        }

        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.VISIBLE

//        viewDataBinding.viewmodel?.getCategory()
//        viewDataBinding.viewmodel?.getCategorySub("")

        setupAdapter()
//        setupObservers()
        listenerCategory()

        getCategory()

    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putString("selectedA",categorySelected)
//        outState.putInt("rvA",recycler_category.scrollX)
//        outState.putInt("rvB",recycler_sub_category.scrollX)
//    }

    private fun setupAdapter() {
        val viewModel = viewDataBinding.viewmodel
        if (viewModel != null) {
            recycler_category?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            recycler_category?.adapter = categoryAdapter

            recycler_sub_category?.layoutManager = GridLayoutManager(activity,2)
            recycler_sub_category?.adapter = categorySubAdapter
        }
    }

    private fun listenerCategory() {
        categoryAdapter.setEventHandler(object: CategoryAdapter.RecyclerClickListener {
            override fun isClicked(id: String, name: String) {
                Log.d("aim","recycler clicked $id")
                category_name?.text = name
//                viewDataBinding.viewmodel?.getCategorySub()
                categorySelected = id
                getCategorySub(categorySelected)
            }
        })
    }

//    private fun setupObservers() {
//        swipe_container?.isRefreshing = true
//        viewDataBinding.viewmodel?.categoryData?.observe(viewLifecycleOwner, Observer {
//            if (it.data.isNotEmpty()) {
//                swipe_container.isRefreshing = false
//                categoryAdapter.setList(it.data)
//
////                viewDataBinding.viewmodel?.getCategorySub(it.data[0].idKategori)
//                category_name?.text = it.data[0].namaKategori
//            } else {
//                swipe_container.isRefreshing = false
//            }
//        })
//
//        viewDataBinding.viewmodel?.categorySubData?.observe(viewLifecycleOwner, Observer {
//            if (it.data.isNotEmpty()) {
//                swipe_container.isRefreshing = false
//                categorySubAdapter.setList(it.data)
//            } else {
//                swipe_container.isRefreshing = false
//            }
//        })
//    }

    private fun getCategory() {
        val listData = mutableListOf<CategoryModel.Data>()

        categoryDB.getAll().observe(viewLifecycleOwner, Observer { data ->

            data.forEach {
                val mCategoryEntity = CategoryModel.Data(it.id,it.image,it.name)
                listData.add(mCategoryEntity)
            }
            val sorting = listData.sortedBy { it.idKategori }
            if (sorting.isNotEmpty()) {
                categorySelected = sorting[0].idKategori
                categoryAdapter.setList(categorySelected, sorting)
            }
//            if (categoryDB.getAll().hasActiveObservers()) {
//                categoryDB.getAll().removeObservers(this)
//            }
        })
    }

    private fun getCategorySub(idCategory: String) {

        val thread = object : Thread() {
            override fun run() {
                val datas = mutableListOf<CategorySubModel.Data>()
                val test = categorySubDB.getById(idCategory)
                test.forEach {
                    val data = CategorySubModel.Data(it.id_category,it.id,it.image,it.name)
                    datas.add(data)
                }

                activity?.runOnUiThread {
                    categorySubAdapter.setList(datas)
                }
            }
        }
        thread.start()

    }


}
