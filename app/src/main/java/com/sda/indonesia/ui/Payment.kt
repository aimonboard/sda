package com.sda.indonesia.ui

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation

import com.sda.indonesia.R
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.payment_fragment.*
import kotlinx.android.synthetic.main.sub_header.*

class Payment : Fragment() {

    private lateinit var viewModel: PaymentViewModel
    private var grandTotal = 0.0F

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.payment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PaymentViewModel::class.java)

        header_name?.text = "Payment"

        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        generatedView()

        btn_bank?.setOnClickListener {
            val bundle = Bundle()
            bundle.putFloat("total",grandTotal)
            Navigation.findNavController(it).navigate(R.id.payBank,bundle)
        }

    }

//    private fun copy() {
//        val clipboard = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
//        val myClip = ClipData.newPlainText("label", virtual.text)
//        clipboard.primaryClip = myClip
//        Toast.makeText(context, "Virtual code copied", Toast.LENGTH_SHORT).show()
//    }

    private fun generatedView() {
        grandTotal = requireArguments().getFloat("total", 0.0F)
        bill_value?.text = FunHelper.bank(grandTotal.toDouble())
    }
}
