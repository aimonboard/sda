package com.sda.indonesia.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sda.indonesia.R
import com.sda.indonesia.data.local.database.category.CategoryViewModelDB
import com.sda.indonesia.ui.adapter.BulkOrderAddAdapter
import com.sda.indonesia.ui.adapter.ListProductAdapter
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.activity_bulk_order_add2.*
import kotlinx.android.synthetic.main.sub_header.*


class BulkOrderAdd2 : AppCompatActivity() {

    private lateinit var categoryVM: CategoryViewModelDB
    private lateinit var listVM: ListViewModel

    private val categoryAdapter = BulkOrderAddAdapter()
    private val productAdapter = ListProductAdapter()

    private var isSetText = false
    private var categorySelected = ""

    var idProduct = ""
    var nameProduct = ""
    var typeProduct = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bulk_order_add2)
        header_name.text = "Add Product"
        categoryVM = ViewModelProviders.of(this).get(CategoryViewModelDB::class.java)
        listVM = ViewModelProviders.of(this).get(ListViewModel::class.java)

        setupAdapter()
        setupObserver()
        listenerCategory()
        listenerProduct()

        search_value?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isNotEmpty() && !isSetText) {
                    loading.visibility = View.VISIBLE
                    if (categorySelected.isNotEmpty()) {
                        listVM.getListProduct(categorySelected, s.toString())
                    } else {
                        listVM.getListProduct(null, s.toString())
                    }
                }
            }
        })

        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_done.setOnClickListener {

            if (emptyCheck()) {
                val returnIntent = Intent()
                returnIntent.putExtra("id", idProduct)
                returnIntent.putExtra("name", nameProduct)
                returnIntent.putExtra("type", typeProduct)
                returnIntent.putExtra("qty", qty_value.text.toString())
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
            }
        }

    }

    private fun emptyCheck(): Boolean{
        var cek = 0

        if(search_value.text.isEmpty()){
            search_lay.error = "Required"; cek++
        } else { search_lay.error = "" }

        if(qty_value.text.isEmpty()){
            qty_lay.error = "Required"; cek++
        } else { qty_lay.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun setupAdapter() {
        recycler_category?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recycler_category?.adapter = categoryAdapter

        recycler_search?.layoutManager = GridLayoutManager(this,2)
        recycler_search?.adapter = productAdapter
    }

    private fun setupObserver() {
        categoryVM.getAll().observe(this, Observer { data ->
            Log.d("aim", "data : $data")
            categoryAdapter.setList(categorySelected, data)
        })

        listVM.listProductData.observe(this, Observer {
            loading.visibility = View.GONE
            if (it.data.isNotEmpty()) {
                tx_err.visibility = View.GONE
                recycler_search.visibility = View.VISIBLE
                productAdapter.reset()
                productAdapter.setList(it.data)
            } else {
                tx_err.visibility = View.VISIBLE
                recycler_search.visibility = View.GONE
                productAdapter.reset()
            }
        })
    }

    private fun listenerCategory() {
        categoryAdapter.setEventHandler(object : BulkOrderAddAdapter.RecyclerClickListener {
            override fun isClicked(id: String, name: String) {
                Log.d("aim", "recycler clicked $id")
                categorySelected = id
            }
        })
    }

    private fun listenerProduct() {
        productAdapter.setEventHandler(object : ListProductAdapter.RecyclerClickListener {
            override fun isClicked(id: String, name: String, type: String) {
                idProduct = id
                nameProduct = name
                typeProduct = type

                recycler_search.visibility = View.GONE
                isSetText = true
                search_value.setText("$name $type")
                isSetText = false
                Log.d("aim", "click: $id, $name")

            }
        })
    }
}