package com.sda.indonesia.ui.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.CategorySubModel
import kotlinx.android.synthetic.main.item_sub_category.view.*

class CategorySubAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<CategorySubModel.Data>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sub_category, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder

        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener { view ->
            val id = vendorList[position].idKategoriSub
            val name = vendorList[position].namaKategoriSub
            val bundle = Bundle()
            bundle.putString("idCategorySub",id)
            bundle.putString("title",name)
            Navigation.findNavController(view).navigate(R.id.listFragment,bundle)
        }
    }
    fun setList(listOfVendor: List<CategorySubModel.Data>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<CategorySubModel.Data>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: CategorySubModel.Data) {
            itemView.tx_name.text = vendorModel.namaKategoriSub
            itemView.photo.setImageResource(R.mipmap.ic_launcher_round)
            itemView.tx_category.visibility = View.GONE
            try { Glide.with(itemView.context).load(vendorModel.imgProduk).into(itemView.photo) }
            catch (e: Exception) { }
        }
    }

}