package com.sda.indonesia.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.midtrans.sdk.corekit.callback.CardRegistrationCallback
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback
import com.midtrans.sdk.corekit.core.MidtransSDK
import com.midtrans.sdk.corekit.core.PaymentMethod
import com.midtrans.sdk.corekit.core.TransactionRequest
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme
import com.midtrans.sdk.corekit.models.BankType
import com.midtrans.sdk.corekit.models.CardRegistrationResponse
import com.midtrans.sdk.corekit.models.CustomerDetails
import com.midtrans.sdk.corekit.models.ItemDetails
import com.midtrans.sdk.corekit.models.snap.CreditCard
import com.midtrans.sdk.corekit.models.snap.TransactionResult
import com.midtrans.sdk.uikit.SdkUIFlowBuilder
import com.sda.indonesia.data.remote.EndPoint

import com.sda.indonesia.R
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.pay_bank.*
import kotlinx.android.synthetic.main.sub_header.*

class PayBank : Fragment(), TransactionFinishedCallback {

    private lateinit var viewModel: PayBankViewModel
    private var grandTotal = 0.0F

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.pay_bank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PayBankViewModel::class.java)

        header_name?.text = "Bank Transfer"
        back_link?.setOnClickListener {
            activity?.onBackPressed()
        }

        generatedView()

        initMidtransSdk()
        initActionButtons()

    }

    private fun generatedView() {
        grandTotal = arguments!!.getFloat("total", 0.0F)
        bill_value?.text = FunHelper.bank(grandTotal.toDouble())
    }

    private fun initTransactionRequest(): TransactionRequest {
        // Create new Transaction Request
        val transactionRequestNew = TransactionRequest("", 20000.00)

        //set customer details
        transactionRequestNew.customerDetails = initCustomerDetails()


        // set item details
        val itemDetails = ItemDetails("1", 20000.00, 1, "Trekking Shoes")

        // Add item details into item detail list.
        val itemDetailsArrayList = ArrayList<ItemDetails>()
        itemDetailsArrayList.add(itemDetails)
        transactionRequestNew.itemDetails = itemDetailsArrayList


        // Create creditcard options for payment
        val creditCard = CreditCard()

        creditCard.isSaveCard = false // when using one/two click set to true and if normal set to  false

//        this methode deprecated use setAuthentication instead
//        creditCard.setSecure(true); // when using one click must be true, for normal and two click (optional)

        creditCard.authentication = CreditCard.AUTHENTICATION_TYPE_3DS

        // noted !! : channel migs is needed if bank type is BCA, BRI or MyBank
//        creditCard.setChannel(CreditCard.MIGS); //set channel migs
        creditCard.bank = BankType.BCA //set spesific acquiring bank

        transactionRequestNew.creditCard = creditCard

        return transactionRequestNew
    }

    private fun initCustomerDetails(): CustomerDetails {
        //define customer detail (mandatory for coreflow)
        val mCustomerDetails = CustomerDetails()
        mCustomerDetails.phone = "085310102020"
        mCustomerDetails.firstName = "user fullname"
        mCustomerDetails.email = "mail@mail.com"
        return mCustomerDetails
    }

    private fun initMidtransSdk() {
        val clientKey = EndPoint.MERCHANT_CLIENT_KEY
        val baseUrl = EndPoint.MERCHANT_BASE_CHECKOUT_URL

        SdkUIFlowBuilder.init()
            .setClientKey(clientKey) // client_key is mandatory
            .setContext(activity) // context is mandatory
            .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
            .setMerchantBaseUrl(baseUrl) //set merchant url
            .enableLog(true) // enable sdk log
            .setColorTheme(CustomColorTheme("#FFE51255", "#B61548", "#FFE51255")) // will replace theme on snap theme on MAP
            .buildSDK()
    }

    override fun onTransactionFinished(result: TransactionResult?) {
        if (result != null) {
            if (result.response != null) {
                when (result.status) {
                    TransactionResult.STATUS_SUCCESS -> Toast.makeText(context, "Transaction Finished. ID: " + result.response.transactionId, Toast.LENGTH_LONG).show()
                    TransactionResult.STATUS_PENDING -> Toast.makeText(context, "Transaction Pending. ID: " + result.response.transactionId, Toast.LENGTH_LONG).show()
                    TransactionResult.STATUS_FAILED -> Toast.makeText(context, "Transaction Failed. ID: " + result.response.transactionId + ". Message: " + result.response.statusMessage, Toast.LENGTH_LONG).show()
                }
                result.response.validationMessages
            } else if (result.isTransactionCanceled) {
                Toast.makeText(context, "Transaction Canceled", Toast.LENGTH_LONG).show()
            }
            else {
                if (result.status.equals(TransactionResult.STATUS_INVALID,true)) {
                    Toast.makeText(context, "Transaction Invalid", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(context, "Transaction Finished with failure.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun initActionButtons() {
//        button_uikit.setOnClickListener {
//            MidtransSDK.getInstance().transactionRequest = initTransactionRequest()
//            MidtransSDK.getInstance().startPaymentUiFlow(context)
//        }
//
//        button_direct_credit_card.setOnClickListener {
//            MidtransSDK.getInstance().transactionRequest = initTransactionRequest()
//            MidtransSDK.getInstance().UiCardRegistration(context!!,object: CardRegistrationCallback {
//                override fun onSuccess(p0: CardRegistrationResponse?) {
//                    Toast.makeText(context, "register card token success", Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onFailure(p0: CardRegistrationResponse?, p1: String?) {
//                    Toast.makeText(context, "register card token Failed", Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onError(p0: Throwable?) {
//                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
//                }
//
//            })
//        }

        btn_bca?.setOnClickListener {
            MidtransSDK.getInstance().transactionRequest = initTransactionRequest()
            MidtransSDK.getInstance().startPaymentUiFlow(context, PaymentMethod.BANK_TRANSFER_BCA)
        }

        btn_mandiri?.setOnClickListener {
            MidtransSDK.getInstance().transactionRequest = initTransactionRequest()
            MidtransSDK.getInstance().startPaymentUiFlow(context, PaymentMethod.BANK_TRANSFER_MANDIRI)
        }

        btn_bni?.setOnClickListener {
            MidtransSDK.getInstance().transactionRequest = initTransactionRequest()
            MidtransSDK.getInstance().startPaymentUiFlow(context, PaymentMethod.BANK_TRANSFER_BNI)
        }

        btn_test?.setOnClickListener {
            MidtransSDK.getInstance().transactionRequest = initTransactionRequest()
            MidtransSDK.getInstance().startPaymentUiFlow(context, PaymentMethod.CREDIT_CARD)
        }
//
//        button_direct_permata_va.setOnClickListener {
//            MidtransSDK.getInstance().transactionRequest = initTransactionRequest()
//            MidtransSDK.getInstance().startPaymentUiFlow(context, PaymentMethod.BANK_TRANSFER_PERMATA)
//        }
//
//        button_direct_atm_bersama_va.setOnClickListener {
//            MidtransSDK.getInstance().transactionRequest = initTransactionRequest()
//            MidtransSDK.getInstance().startPaymentUiFlow(context, PaymentMethod.BCA_KLIKPAY)
//        }

    }

}
