package com.sda.indonesia.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sda.indonesia.MainActivity

import com.sda.indonesia.R
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.data.local.database.cart.CartConverterModel
import com.sda.indonesia.data.local.database.cart.CartEntity
import com.sda.indonesia.data.local.database.cart.CartViewModelDB
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.AssemblySummaryVM
import kotlinx.android.synthetic.main.assembly_summary_fragment.*

class AssemblySummaryFragment : Fragment() {

    private lateinit var viewModel: AssemblySummaryVM
    private lateinit var cartDB: CartViewModelDB

    private var cartID = ArrayList<String>()
    private var cartWeight = ArrayList<Int>()
    private var cartPrice = ArrayList<Double>()

    private var mCartSubData = mutableListOf<CartConverterModel>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.assembly_summary_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AssemblySummaryVM::class.java)
        cartDB = ViewModelProviders.of(this@AssemblySummaryFragment).get(CartViewModelDB::class.java)
        val navBar = activity?.findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar?.visibility = View.GONE

        back_link.setOnClickListener {
            requireActivity().onBackPressed()
        }

        btn_prev.setOnClickListener {
            requireActivity().onBackPressed()
        }

        val loginStatus = SharedPreferenceHelper.getSharedPreferenceString(activity,1,"")
        btn_next.setOnClickListener {
            if (loginStatus == "1") {

                addToSubCart()
                addToCart()

                val bundle = Bundle()
                bundle.putString("from","1")
                Navigation.findNavController(it).navigate(R.id.cartFragment,bundle)

            } else {
                val bundle = Bundle()
                bundle.putString("goto","assembly")
                Navigation.findNavController(it).navigate(R.id.loginFragment,bundle)
            }
        }

        generateView()
    }

    private fun generateView() {
        cartID.clear()
        cartWeight.clear()
        cartPrice.clear()
        mCartSubData.clear()

        cartID.add(MainActivity.hoseVariant?.idSubProduk ?: "")
        cartWeight.add(5)
        cartPrice.add(MainActivity.hoseVariant?.hargaProduk ?: 0.0)
        //-ferull

        hose_name.text = MainActivity.hose?.namaProduk ?: ""
        hose_length.text = MainActivity.hoseVariant?.length ?: ""
        hose_diameter.text = MainActivity.hoseVariant?.dash ?: ""
        hose_price.text = FunHelper.bank(MainActivity.hoseVariant?.hargaProduk ?: 0.0)
        try { Glide.with(requireContext()).load(MainActivity.hose!!.imgProduk).into(hose_photo) } catch (e: Exception) { }

        cartID.add(MainActivity.fitting1Variant?.idSubProduk ?: "")
        cartWeight.add(5)
        cartPrice.add(MainActivity.fitting1Variant?.hargaProduk ?: 0.0)
        //-ferull

        fitting1_name.text = MainActivity.fitting1?.namaProduk ?: ""
        fitting1_diameter.text = MainActivity.fitting1Variant?.dash ?:""
        fitting1_price.text = FunHelper.bank(MainActivity.fitting1Variant?.hargaProduk ?: 0.0)
        try { Glide.with(requireContext()).load(MainActivity.fitting1!!.imgProduk).into(fitting1_photo) } catch (e: Exception) { }

        cartID.add(MainActivity.fitting2Variant?.idSubProduk ?: "")
        cartWeight.add(5)
        cartPrice.add(MainActivity.fitting2Variant?.hargaProduk ?: 0.0)
        //-ferull

        fitting2_name.text = MainActivity.fitting2?.namaProduk ?: ""
        fitting2_diameter.text = MainActivity.fitting2Variant?.dash ?: ""
        fitting2_price.text = FunHelper.bank(MainActivity.fitting2Variant?.hargaProduk ?: 0.0)
        try { Glide.with(requireContext()).load(MainActivity.fitting2!!.imgProduk).into(fitting2_photo) } catch (e: Exception) { }
    }

    private fun addToSubCart() {
        // add custom part to sub cart database
        val mHose =
            CartConverterModel(
                "custom",
                MainActivity.hose?.idProduk ?: "",
                "${MainActivity.hose?.namaProduk} ${MainActivity.hose?.produkType}",
                MainActivity.hose?.namaKategori ?: "",
                MainActivity.hose?.imgProduk ?: "",
                MainActivity.hoseVariant?.idSubProduk ?: "",
                MainActivity.hoseVariant?.dash ?: "",
                "",
                MainActivity.hoseVariant?.size ?: "",
                MainActivity.hoseVariant?.namesize ?: "",
                MainActivity.hoseVariant?.length ?: "",
                MainActivity.hoseVariant?.namelength ?: "",
                MainActivity.hoseVariant?.pressure ?: "",
                "",
                MainActivity.hoseVariant?.weight ?: 0,
                MainActivity.hoseVariant?.nameweight ?: "",
                MainActivity.hoseVariant?.hargaProduk ?: 0.0,
                "",
                MainActivity.hoseVariant?.stokProduk ?: 0,
                ""
            )
        mCartSubData.add(mHose)
        Log.d("aim", "main hose : $mHose")


        val mFitting1 =
            CartConverterModel(
                "custom",
                MainActivity.fitting1?.idProduk ?: "",
                "${MainActivity.fitting1?.namaProduk} ${MainActivity.hose?.produkType}",
                MainActivity.fitting1?.namaKategori ?: "",
                MainActivity.fitting1?.imgProduk ?: "",
                MainActivity.fitting1Variant?.idSubProduk ?: "",
                MainActivity.fitting1Variant?.dash ?: "",
                "",
                MainActivity.fitting1Variant?.size ?: "",
                MainActivity.fitting1Variant?.namesize ?: "",
                MainActivity.fitting1Variant?.length ?: "",
                MainActivity.fitting1Variant?.namelength ?: "",
                MainActivity.fitting1Variant?.pressure ?: "",
                "",
                MainActivity.fitting1Variant?.weight ?: 0,
                MainActivity.fitting1Variant?.nameweight ?: "",
                MainActivity.fitting1Variant?.hargaProduk ?: 0.0,
                "",
                MainActivity.fitting1Variant?.stokProduk ?: 0,
                ""
            )
        mCartSubData.add(mFitting1)
        Log.d("aim", "main fitting 1 : $mFitting1")

        val mFitting2 =
            CartConverterModel(
                "custom",
                MainActivity.fitting2?.idProduk ?: "",
                "${MainActivity.fitting2?.namaProduk} ${MainActivity.hose?.produkType}",
                MainActivity.fitting2?.namaKategori ?: "",
                MainActivity.fitting2?.imgProduk ?: "",
                MainActivity.fitting2Variant?.idSubProduk ?: "",
                MainActivity.fitting2Variant?.dash ?: "",
                "",
                MainActivity.fitting2Variant?.size ?: "",
                MainActivity.fitting2Variant?.namesize ?: "",
                MainActivity.fitting2Variant?.length ?: "",
                MainActivity.fitting2Variant?.namelength ?: "",
                MainActivity.fitting2Variant?.pressure ?: "",
                "",
                MainActivity.fitting2Variant?.weight ?: 0,
                MainActivity.fitting2Variant?.nameweight ?: "",
                MainActivity.fitting2Variant?.hargaProduk ?: 0.0,
                "",
                MainActivity.fitting2Variant?.stokProduk ?: 0,
                ""
            )
        mCartSubData.add(mFitting2)
        Log.d("aim", "main fitting 2 : $mFitting2.")
    }

    private fun addToCart() {
        // add custom to main cart database
        val mCart = CartEntity(
            FunHelper.randomString(),
            mCartSubData,
            "",
            "Custom Assembly",
            "SDA Hydraulics",
            "https://sdaindonesia.com/beta/assets/img/logo.png",
            FunHelper.randomString(),
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            cartWeight.sum(),
            "",
            cartPrice.sum(),
            "",
            0,
            ""
        )
        cartDB.insert(mCart)
        Log.d("aim", "main cart : $mCart")
    }



}
