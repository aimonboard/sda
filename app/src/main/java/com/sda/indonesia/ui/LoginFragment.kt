package com.sda.indonesia.ui

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task

import com.sda.indonesia.R
import kotlinx.android.synthetic.main.login_fragment.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.sda.indonesia.data.local.SharedPreferenceHelper
import com.sda.indonesia.data.model.sda.LoginModel
import com.sda.indonesia.data.remote.ApiClient
import com.sda.indonesia.data.remote.EndPoint
import com.sda.indonesia.ui.helper.FunHelper
import com.sda.indonesia.viewmodel.LoginViewModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginFragment : Fragment() {

    lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var viewModel: LoginViewModel
    private val loginReq = 9001

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        val navBar = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_nav)
        navBar.visibility = View.GONE

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(EndPoint.oAuthWeb)
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)

        btnLogin.setOnClickListener {
            if (emptyCheck()) {
                SharedPreferenceHelper.setSharedPreferenceString(context,1,"1")
                login()
            }
        }


        btnSignup.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.registerFragment)
        }

//        requireActivity().onBackPressedDispatcher.addCallback(this,object: OnBackPressedCallback(true) {
//            override fun handleOnBackPressed() {
//                Log.d("aim","test back from fragment")
////                Navigation.findNavController(view).previousBackStackEntry?.savedStateHandle?.set("backStack", "0")
//                Navigation.findNavController(view).popBackStack()
//            }
//
//        })


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == loginReq) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(
                ApiException::class.java
            )
            // Signed in successfully
            val googleId = account?.id ?: ""
            Log.i("Google ID",googleId)

            val googleFirstName = account?.givenName ?: ""
            Log.i("Google First Name", googleFirstName)

            val googleLastName = account?.familyName ?: ""
            Log.i("Google Last Name", googleLastName)

            val googleEmail = account?.email ?: ""
            Log.i("Google Email", googleEmail)

            val googleProfilePicURL = account?.photoUrl.toString()
            Log.i("Google Profile Pic URL", googleProfilePicURL)

            val googleIdToken = account?.idToken ?: ""
            Log.i("Google ID Token", googleIdToken)


            signInSuccess()


        } catch (e: ApiException) {
            // Sign in was unsuccessful
            Navigation.findNavController(requireView()).popBackStack()
            Log.e("failed code=", e.statusCode.toString())
        }
    }

    private fun emptyCheck(): Boolean{
        var cek = 0

        if(user.text.isEmpty()){
            user_lay.error = "Required"; cek++
        } else { user_lay.error = "" }

        if(pass.text.isEmpty()){
            pass_lay.error = "Required"; cek++
        } else { pass_lay.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun login() {
        loading.visibility = View.VISIBLE
//        btnRegister.isEnabled = false

        val params = HashMap<String, String>()
        params["email"] = user.text.toString()
        params["password"] = pass.text.toString()

        ApiClient.instance.loginRequest(params).enqueue(object: Callback<LoginModel> {
            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>?) {
                if (response != null && response.isSuccessful) {
//                    val ikiTokenCok = response.body()?.
                    SharedPreferenceHelper.setSharedPreferenceString(context,2, response.body()?.idToken ?: "")
                    SharedPreferenceHelper.setSharedPreferenceString(context,3, response.body()?.id ?: "")
                    SharedPreferenceHelper.setSharedPreferenceString(context,4, response.body()?.nama ?: "")

                    signInSuccess()
                }
                else {
                    try {
                        val jObjError = JSONObject(response!!.errorBody()!!.string())
                        FunHelper.snackBar(root_lay, jObjError.getString("message"))
                    } catch (e: Exception) { }
                }
                loading.visibility = View.GONE
//                btnRegister.isEnabled = true
            }

            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                loading.visibility = View.GONE
//                btnRegister.isEnabled = true
            }

        })
    }

    private fun signInSuccess() {
        when (arguments?.getString("goto")) {
            "cart" -> requireActivity().onBackPressed()
            "profile" -> Navigation.findNavController(requireView()).navigate(R.id.action_loginFragment_to_profileFragment)
            else -> Navigation.findNavController(requireView()).popBackStack()
        }
    }



}
