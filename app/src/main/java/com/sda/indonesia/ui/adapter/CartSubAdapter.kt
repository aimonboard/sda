package com.sda.indonesia.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.local.database.cart.CartConverterModel
import com.sda.indonesia.data.model.sda.ProductSubModel
import com.sda.indonesia.ui.helper.FunHelper
import kotlinx.android.synthetic.main.item_cart_sub.view.*

class CartSubAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<CartConverterModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_cart_sub, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        val mData = vendorList[position]

        holder.itemView.setOnClickListener {
            if (holder.itemView.expandable_layout.isExpanded) {
                holder.itemView.btn_down.rotation = 0F
                holder.itemView.expandable_layout.isExpanded = false
            } else {
                holder.itemView.btn_down.rotation = 180F
                holder.itemView.expandable_layout.isExpanded = true
            }
        }

    }

    fun setList(listOfVendor: List<CartConverterModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: CartConverterModel) {
            itemView.tx_name.text = vendorModel.product_name

            var dash = vendorModel.sub_dash
            dash += vendorModel.sub_dash_uom
            itemView.dash_value.text = dash.ifBlank { "-" }

            var diameter = vendorModel.sub_height
            diameter += " ${vendorModel.sub_height_uom}"
            itemView.diameter_value.text = diameter.ifBlank { "-" }

            var length = vendorModel.sub_length
            length += " ${vendorModel.sub_length_uom}"
            itemView.length_value.text = length.ifBlank { "-" }

            var pressure = vendorModel.sub_pressure
            pressure += " ${vendorModel.sub_pressure_uom}"
            itemView.pres_value.text = pressure.ifBlank { "-" }

            var weight: String = vendorModel.sub_weight.toString()
            weight += " ${vendorModel.sub_weight_uom}"
            itemView.weight_value.text = weight.ifBlank { "-" }

            itemView.harga_value.text = FunHelper.bank(vendorModel.sub_price)
        }
    }

}