package com.sda.indonesia.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sda.indonesia.R
import com.sda.indonesia.data.model.sda.TransactionModel
import com.sda.indonesia.ui.adapter.TransactionAdapter
import kotlinx.android.synthetic.main.fragment_transaction.*
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.sub_header.back_link
import kotlinx.android.synthetic.main.sub_header.header_name

class Transaction : Fragment() {

    private val adapter = TransactionAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle? ): View? {
        return inflater.inflate(R.layout.fragment_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        header_name.text = "Transaction"

        setAdapter()

        back_link.setOnClickListener {
            requireActivity().onBackPressed()
        }

    }

    private fun setAdapter() {
        recycler_transaction?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_transaction?.adapter = adapter

        adapter.add(TransactionModel("","0"))
        adapter.add(TransactionModel("","1"))
        adapter.add(TransactionModel("","2"))
    }
}