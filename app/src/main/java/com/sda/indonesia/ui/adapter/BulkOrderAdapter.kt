package com.sda.indonesia.ui.adapter

import android.app.AlertDialog
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sda.indonesia.R
import com.sda.indonesia.data.local.database.cart.CartEntity
import com.sda.indonesia.data.model.sda.BulkOrderModel
import kotlinx.android.synthetic.main.item_bulk_order.view.*

class BulkOrderAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<BulkOrderModel>()
    private var eventHandler: RecyclerClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_bulk_order, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder
        holder.bindView(vendorList[position])

        val id = vendorList[position].id
        val name = vendorList[position].name

        holder.itemView.setOnClickListener { view ->
            eventHandler?.isClicked(id,name)
        }

        holder.itemView.setOnClickListener {
            AlertDialog.Builder(it.context)
                .setMessage("Delete this item ?")
                .setPositiveButton(android.R.string.yes) { dialog, whichButton ->
                    delete(vendorList[position])
                }
                .setNegativeButton(android.R.string.no, null)
                .show()
        }

    }

    fun setList(listOfVendor: List<BulkOrderModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: BulkOrderModel) {
        this.vendorList.add(data)
        notifyDataSetChanged()
    }

    fun delete(data: BulkOrderModel) {
        this.vendorList.remove(data)
        notifyDataSetChanged()
    }

    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: BulkOrderModel) {
            itemView.tx_name.text = vendorModel.name
            itemView.tx_qty.text = "Quantity: ${vendorModel.qty}"
        }
    }


    fun setEventHandler(eventHandler: RecyclerClickListener) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface RecyclerClickListener {
        fun isClicked(id: String, name: String)
    }

}