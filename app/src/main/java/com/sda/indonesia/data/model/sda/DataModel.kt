package com.sda.indonesia.data.model.sda

data class DataModel (
    val id_vendor   :String,
    val nama        :String,
    val photo       :String
)