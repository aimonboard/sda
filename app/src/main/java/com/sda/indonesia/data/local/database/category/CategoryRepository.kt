package com.sda.indonesia.data.local.database.category

import android.app.Application
import androidx.lifecycle.LiveData

class CategoryRepository constructor(application: Application) {

    lateinit var dao : CategoryDao
    lateinit var data : LiveData<List<CategoryEntity>>

    init {
        val database = CategoryDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.getAll()
        }
    }

    fun getAll() : LiveData<List<CategoryEntity>> {
        return data
    }

    fun insert(entity: CategoryEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.insert(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.deleteAll()
            }
        }
        thread.start()
    }

}