package com.sda.indonesia.data.local.database.cart

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*


class CartConverter {
    var gson = Gson()

    @TypeConverter
    fun stringToSomeObjectList(data: String?): List<CartConverterModel?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<List<CartConverterModel?>?>() {}.type
        return gson.fromJson<List<CartConverterModel?>>(data, listType)
    }

    @TypeConverter
    fun someObjectListToString(someObjects: List<CartConverterModel?>?): String? {
        return gson.toJson(someObjects)
    }


}