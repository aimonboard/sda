package com.sda.indonesia.data.local.database.city

import android.app.Application
import androidx.lifecycle.LiveData

class CityRepository constructor(application: Application) {

    lateinit var dao : CityDao
    lateinit var data : LiveData<List<CityEntity>>

    init {
        val database = CityDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.getAll()
        }
    }

    fun getAll() : LiveData<List<CityEntity>> {
        return data
    }

    fun insert(entity: CityEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.insert(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.deleteAll()
            }
        }
        thread.start()
    }

}