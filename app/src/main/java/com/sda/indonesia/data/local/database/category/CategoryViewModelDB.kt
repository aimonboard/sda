package com.sda.indonesia.data.local.database.category

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class CategoryViewModelDB constructor(application: Application) : AndroidViewModel(application) {
    private val repository : CategoryRepository = CategoryRepository(application)
    private val data : LiveData<List<CategoryEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<CategoryEntity>> {
        return data
    }

    fun insert(data: CategoryEntity){
        repository.insert(data)
    }

//    fun upsert(data: KelasEntity){
//        repository.upsert(data)
//    }

    fun deleteAll(){
        repository.deleteAll()
    }

}