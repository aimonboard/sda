package com.sda.indonesia.data.local.database.city

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [CityEntity::class],version = 1, exportSchema = false)
abstract class CityDatabase : RoomDatabase() {

    abstract fun dao(): CityDao

    companion object {
        var instance: CityDatabase? = null
        fun getInstance(context: Context): CityDatabase? {
            if (instance == null) {
                synchronized(CityDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        CityDatabase::class.java, "table_city")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return instance
        }
    }

}