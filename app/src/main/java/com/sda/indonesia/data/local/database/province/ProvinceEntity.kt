package com.sda.indonesia.data.local.database.province

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "table_province")
class ProvinceEntity (
    @PrimaryKey(autoGenerate = false)
    val id : String,
    val name : String
)