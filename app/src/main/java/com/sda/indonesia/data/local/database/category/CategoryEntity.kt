package com.sda.indonesia.data.local.database.category

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "table_category")
class CategoryEntity (
    @PrimaryKey(autoGenerate = false)
    val id : String,
    val name : String,
    val image : String
)