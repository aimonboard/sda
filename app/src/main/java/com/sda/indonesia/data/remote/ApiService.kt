package com.sda.indonesia.data.remote

import com.sda.indonesia.data.model.rajaongkir.CityModel
import com.sda.indonesia.data.model.rajaongkir.ProvinceModel
import com.sda.indonesia.data.model.rajaongkir.ShippingCostModel
import com.sda.indonesia.data.model.sda.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @POST("api/v2/content-blog")
    fun getRepo(@Body params: JSONObject): Call<DataRepo>

    @FormUrlEncoded
    @POST("adminsda/api/authentication/registration")
    fun registerRequest(
        @FieldMap body: HashMap<String,String>
    ): Call<RegisterModel>

    @FormUrlEncoded
    @POST("adminsda/api/login")
    fun loginRequest(
        @FieldMap body: HashMap<String,String>
    ): Call<LoginModel>

    @GET("adminsda/api/kategori")
    fun getCategory(): Call<CategoryModel>

    @GET("adminsda/api/SubKategori")
    fun getCategorySub(): Call<CategorySubModel>

    @GET("adminsda/api/brand/")
    fun getBrand(): Call<BrandModel>

//    @GET("adminsda/api/produk")
//    fun getListProduct(@Query("id_kategori_sub") idCategorySub: String): Call<ProductModel>

    @GET("adminsda/api/produk")
    fun getListProduct(
        @Query("id_kategori_sub") idCategorySub: String?,
        @Query("nama_produk") produkName: String?
    ): Call<ProductModel>

    @GET("adminsda/api/produk")
    fun getProductDetail(@Query("id_produk") idProduct: String): Call<ProductModel>

    @GET("adminsda/api/produk")
    fun getProductSub(@Query("id_produk_fk") idProduct: String): Call<ProductSubModel>



    // =================================================================================================================

    @Headers ("key:${EndPoint.key}")
    @GET
    fun getProvince(
        @Url url: String
    ): Call<ProvinceModel>

    @Headers ("key:${EndPoint.key}")
    @GET
    fun getCity(
        @Url url: String
    ): Call<CityModel>

    @Headers ("key:${EndPoint.key}")
    @FormUrlEncoded
    @POST
    fun getShippingCost(
        @Url url: String,
        @FieldMap body: HashMap<String,String>
    ): Call<ShippingCostModel>








}