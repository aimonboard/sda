package com.sda.indonesia.data.model.sda


import com.google.gson.annotations.SerializedName

data class BrandModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("http_status")
    val httpStatus: Int,
    @SerializedName("status")
    val status: Boolean
) {
    data class Data(
        @SerializedName("created_at")
        val createdAt: String,
        @SerializedName("id_brand")
        val idBrand: String,
        @SerializedName("nama_brand")
        val namaBrand: String
    )
}