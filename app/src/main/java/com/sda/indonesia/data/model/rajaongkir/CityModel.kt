package com.sda.indonesia.data.model.rajaongkir


import com.google.gson.annotations.SerializedName

data class CityModel(
    @SerializedName("rajaongkir")
    val rajaongkir: Rajaongkir
) {
    data class Rajaongkir(
        @SerializedName("query")
        val query: List<Any>,
        @SerializedName("results")
        val results: List<Result>,
        @SerializedName("status")
        val status: Status
    ) {
        data class Result(
            @SerializedName("city_id")
            val cityId: String,
            @SerializedName("city_name")
            val cityName: String,
            @SerializedName("postal_code")
            val postalCode: String,
            @SerializedName("province")
            val province: String,
            @SerializedName("province_id")
            val provinceId: String,
            @SerializedName("type")
            val type: String
        )

        data class Status(
            @SerializedName("code")
            val code: Int,
            @SerializedName("description")
            val description: String
        )
    }
}