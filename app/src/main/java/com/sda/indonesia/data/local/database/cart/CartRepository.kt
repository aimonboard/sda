package com.sda.indonesia.data.local.database.cart

import android.app.Application
import androidx.lifecycle.LiveData

class CartRepository constructor(application: Application) {

    lateinit var dao : CartDao
    lateinit var data : LiveData<List<CartEntity>>

    init {
        val database = CartDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.getAll()
        }
    }

    fun getAll() : LiveData<List<CartEntity>> {
        return data
    }

    fun insert(entity: CartEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.insert(entity)
            }
        }
        thread.start()
    }

    fun delete(entity: CartEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.delete(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.deleteAll()
            }
        }
        thread.start()
    }

}