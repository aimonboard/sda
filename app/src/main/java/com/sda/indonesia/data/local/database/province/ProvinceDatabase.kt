package com.sda.indonesia.data.local.database.province

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [ProvinceEntity::class],version = 1, exportSchema = false)
abstract class ProvinceDatabase : RoomDatabase() {
    abstract fun dao(): ProvinceDao

    companion object {
        var instance: ProvinceDatabase? = null
        fun getInstance(context: Context): ProvinceDatabase? {
            if (instance == null) {
                synchronized(ProvinceDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        ProvinceDatabase::class.java, "table_province")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return instance
        }
    }
}