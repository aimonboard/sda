package com.sda.indonesia.data.model.sda


import com.google.gson.annotations.SerializedName

data class RegisterModel(
    @SerializedName("status")
    val status: Boolean = false,
    @SerializedName("message")
    val message: String = ""
)