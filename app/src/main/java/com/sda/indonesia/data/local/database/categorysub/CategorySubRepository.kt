package com.sda.indonesia.data.local.database.categorysub

import android.app.Application
import androidx.lifecycle.LiveData

class CategorySubRepository constructor(application: Application) {

    lateinit var dao : CategorySubDao
    lateinit var data : LiveData<List<CategorySubEntity>>

    init {
        val database = CategorySubDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.getAll()
        }
    }

    fun getAll() : LiveData<List<CategorySubEntity>> {
        return data
    }

    fun getById(id: String) : List<CategorySubEntity> {
        return dao.getById(id)
    }

    fun insert(entity: CategorySubEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.insert(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.deleteAll()
            }
        }
        thread.start()
    }

}