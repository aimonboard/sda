package com.sda.indonesia.data.local.database.cart

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class CartViewModelDB constructor(application: Application) : AndroidViewModel(application) {
    private val repository : CartRepository = CartRepository(application)
    private val data : LiveData<List<CartEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<CartEntity>> {
        return data
    }

    fun insert(data: CartEntity){
        repository.insert(data)
    }

    fun delete(entity: CartEntity){
        repository.delete(entity)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

}