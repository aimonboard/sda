package com.sda.indonesia.data.local

import android.R.id.edit
import android.content.Context
import android.content.SharedPreferences



class SharedPreferenceHelper {

    companion object {

        private const val PREF_FILE = "sda_data"

        private fun generateKey(keyCode: Int): String? {
            return when (keyCode) {
                // Credentials
                1 -> "login_status"
                2 -> "token"
                3 -> "user_id"
                4 -> "name"

                // Filter
                11 -> "filter_Status"
                22 -> "category"
                33 -> "category_sub"
                44 -> "brand"

                // Shipping Address
                111 -> "address"
                222 -> "post"
                333 -> "city"
                444 -> "city_id"
                555 -> "province"

                // Shipping Contact
                1111 -> "name"
                2222 -> "phone"

                else -> null
            }
        }

        fun setSharedPreferenceString(context: Context?, key: Int, value: String) {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            val editor = settings?.edit()
            editor?.putString(generateKey(key), value)
            editor?.apply()
        }

        fun getSharedPreferenceString(context: Context?, key: Int, defValue: String): String? {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            return settings?.getString(generateKey(key), defValue)
        }

    }

}