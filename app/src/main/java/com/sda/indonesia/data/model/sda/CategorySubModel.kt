package com.sda.indonesia.data.model.sda


import com.google.gson.annotations.SerializedName

data class CategorySubModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("http_status")
    val httpStatus: Int,
    @SerializedName("status")
    val status: Boolean
) {
    data class Data(
        @SerializedName("id_kategori")
        val idKategori: String,
        @SerializedName("id_kategori_sub")
        val idKategoriSub: String,
        @SerializedName("img_produk")
        val imgProduk: String,
        @SerializedName("nama_kategori_sub")
        val namaKategoriSub: String
    )
}