package com.sda.indonesia.data.local.database.categorysub

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class CategorySubViewModelDB constructor(application: Application) : AndroidViewModel(application) {
    private val repository : CategorySubRepository = CategorySubRepository(application)
    private val data : LiveData<List<CategorySubEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<CategorySubEntity>> {
        return data
    }

    fun getById(id: String): List<CategorySubEntity> {
        return repository.getById(id)
    }

    fun insert(data: CategorySubEntity){
        repository.insert(data)
    }

//    fun upsert(data: KelasEntity){
//        repository.upsert(data)
//    }

    fun deleteAll(){
        repository.deleteAll()
    }

}