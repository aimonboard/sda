package com.sda.indonesia.data.local.database.categorysub

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CategorySubDao {

    @Query("SELECT * from table_category_sub")
    fun getAll(): LiveData<List<CategorySubEntity>>

    @Query("SELECT * from table_category_sub WHERE id_category =:uom_id")
    fun getById(uom_id: String): List<CategorySubEntity>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(uom: CategorySubEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(entity: CategorySubEntity)

    @Delete
    fun delete(uom: CategorySubEntity)

//    fun upsert(entity: CategorySubEntity) {
//        try {
//            insert(entity)
//        } catch (exception: Exception) {
//            update(entity)
//        }
//    }


    @Query("DELETE FROM table_category_sub")
    fun deleteAll()

}