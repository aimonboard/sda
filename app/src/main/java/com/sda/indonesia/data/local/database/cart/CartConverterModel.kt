package com.sda.indonesia.data.local.database.cart

data class CartConverterModel (
    val id: String,
    val product_id: String,
    val product_name: String,
    val product_category: String,
    val product_img: String,
    val sub_id: String,
    val sub_dash: String,
    val sub_dash_uom: String,
    val sub_height: String,
    val sub_height_uom: String,
    val sub_length: String,
    val sub_length_uom: String,
    val sub_pressure: String,
    val sub_pressure_uom: String,
    val sub_weight: Int,
    val sub_weight_uom: String,
    val sub_price: Double,
    val sub_price_uom: String,
    val sub_stock: Int,
    val sub_stock_uom: String
)