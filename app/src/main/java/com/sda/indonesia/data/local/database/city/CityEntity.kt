package com.sda.indonesia.data.local.database.city

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "table_city")
class CityEntity (
    @PrimaryKey(autoGenerate = false)
    val city_id : String,
    val province_id : String,
    val province : String,
    val type : String,
    val city_name : String,
    val postal_code : String
)