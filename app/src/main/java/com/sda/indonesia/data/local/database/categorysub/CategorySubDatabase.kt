package com.sda.indonesia.data.local.database.categorysub

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database (entities = [CategorySubEntity::class],version = 2, exportSchema = false)
abstract class CategorySubDatabase : RoomDatabase() {

    abstract fun dao(): CategorySubDao

    companion object {
        var instance: CategorySubDatabase? = null
        fun getInstance(context: Context): CategorySubDatabase? {
            if (instance == null) {
                synchronized(CategorySubDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        CategorySubDatabase::class.java, "table_category_sub")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return instance
        }
    }
}