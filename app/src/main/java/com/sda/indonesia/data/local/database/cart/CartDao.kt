package com.sda.indonesia.data.local.database.cart

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CartDao {

    @Query("SELECT * from table_cart")
    fun getAll(): LiveData<List<CartEntity>>

    @Query("SELECT * from table_cart WHERE id =:id")
    fun getById(id: String): List<CartEntity>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(uom: CartEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(entity: CartEntity)

    @Delete
    fun delete(uom: CartEntity)

//    fun upsert(entity: CartEntity) {
//        try {
//            insert(entity)
//        } catch (exception: Exception) {
//            update(entity)
//        }
//    }


    @Query("DELETE FROM table_cart")
    fun deleteAll()


}