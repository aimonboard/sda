package com.sda.indonesia.data.local.database.city

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class CityViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : CityRepository = CityRepository(application)
    private val data : LiveData<List<CityEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<CityEntity>> {
        return data
    }

    fun insert(data: CityEntity){
        repository.insert(data)
    }

//    fun upsert(data: KelasEntity){
//        repository.upsert(data)
//    }

    fun deleteAll(){
        repository.deleteAll()
    }

}