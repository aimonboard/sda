package com.sda.indonesia.data.local.database.categorysub

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "table_category_sub")
class CategorySubEntity (
    @PrimaryKey(autoGenerate = false)
    val id : String,
    val id_category : String,
    val name : String,
    val image : String
)