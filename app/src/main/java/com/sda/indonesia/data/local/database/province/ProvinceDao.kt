package com.sda.indonesia.data.local.database.province

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ProvinceDao {
    @Query("SELECT * from table_province")
    fun getAll(): LiveData<List<ProvinceEntity>>

    @Query("SELECT * from table_province WHERE id =:id")
    fun getById(id: String): List<ProvinceEntity>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(uom: ProvinceEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(entity: ProvinceEntity)

    @Delete
    fun delete(uom: ProvinceEntity)

//    fun upsert(entity: ProvinceEntity) {
//        try {
//            insert(entity)
//        } catch (exception: Exception) {
//            update(entity)
//        }
//    }


    @Query("DELETE FROM table_province")
    fun deleteAll()

}