package com.sda.indonesia.data.remote

import com.google.gson.JsonObject
import com.sda.indonesia.data.*
import com.sda.indonesia.data.model.rajaongkir.CityModel
import com.sda.indonesia.data.model.rajaongkir.ProvinceModel
import com.sda.indonesia.data.model.rajaongkir.ShippingCostModel
import com.sda.indonesia.data.model.sda.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Body

class RepoRepository {

    companion object {
        private var INSTANCE: RepoRepository? = null
        fun getInstance() = INSTANCE
            ?: RepoRepository().also {
                INSTANCE = it
            }
    }

    // GET repo list
    fun getRepoList(onResult: (isSuccess: Boolean, response: DataRepo?) -> Unit) {

        val body = JSONObject()
        body.put("category","2")

        ApiClient.instance.getRepo(body).enqueue(object : Callback<DataRepo> {
            override fun onResponse(call: Call<DataRepo>?, response: Response<DataRepo>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<DataRepo>?, t: Throwable?) {
                onResult(false, null)
            }

        })
    }



    fun getCategory(onResult: (isSuccess: Boolean, response: CategoryModel?) -> Unit) {
        ApiClient.instance.getCategory().enqueue(object : Callback<CategoryModel> {
            override fun onResponse(call: Call<CategoryModel>?, response: Response<CategoryModel>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }
            override fun onFailure(call: Call<CategoryModel>?, t: Throwable?) {
                onResult(false, null)
            }
        })
    }

    fun getCategorySub(onResult: (isSuccess: Boolean, response: CategorySubModel?) -> Unit) {
        ApiClient.instance.getCategorySub().enqueue(object : Callback<CategorySubModel> {
            override fun onResponse(call: Call<CategorySubModel>?, response: Response<CategorySubModel>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }
            override fun onFailure(call: Call<CategorySubModel>?, t: Throwable?) {
                onResult(false, null)
            }
        })
    }

    fun getBrand(onResult: (isSuccess: Boolean, response: BrandModel?) -> Unit) {
        ApiClient.instance.getBrand().enqueue(object : Callback<BrandModel> {
            override fun onResponse(call: Call<BrandModel>?, response: Response<BrandModel>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }
            override fun onFailure(call: Call<BrandModel>?, t: Throwable?) {
                onResult(false, null)
            }
        })
    }

    fun getListProduct(
        idCategorySub: String?,
        produkName: String?,
        onResult: (isSuccess: Boolean, response: ProductModel?) -> Unit) {

        ApiClient.instance.getListProduct(
            idCategorySub,
            produkName
        ).enqueue(object : Callback<ProductModel> {
            override fun onResponse(call: Call<ProductModel>?, response: Response<ProductModel>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }
            override fun onFailure(call: Call<ProductModel>?, t: Throwable?) {
                onResult(false, null)
            }
        })
    }

    fun getProductDetail(idProduct: String, onResult: (isSuccess: Boolean, response: ProductModel?) -> Unit) {
        ApiClient.instance.getProductDetail(idProduct).enqueue(object: Callback<ProductModel> {
            override fun onResponse(call: Call<ProductModel>, response: Response<ProductModel>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<ProductModel>, t: Throwable) {
                onResult(false, null)
            }

        })
    }

    fun getProductSub(idProduct: String, onResult: (isSuccess: Boolean, response: ProductSubModel?) -> Unit) {
        ApiClient.instance.getProductSub(idProduct).enqueue(object: Callback<ProductSubModel> {
            override fun onResponse(call: Call<ProductSubModel>, response: Response<ProductSubModel>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<ProductSubModel>, t: Throwable) {
                onResult(false, null)
            }

        })
    }


    // =================================================================================================================

    fun getProvince(url: String, onResult: (isSuccess: Boolean, response: ProvinceModel?) -> Unit) {
        ApiClient.instance.getProvince(url).enqueue(object: Callback<ProvinceModel> {
            override fun onResponse(call: Call<ProvinceModel>, response: Response<ProvinceModel>?) {
                if (response != null)
                    onResult(true, response.body()!!)
            }

            override fun onFailure(call: Call<ProvinceModel>, t: Throwable) {
                onResult(false, null)
            }

        })
    }

    fun getCity(url: String, onResult: (isSuccess: Boolean, response: CityModel?) -> Unit) {
        ApiClient.instance.getCity(url).enqueue(object: Callback<CityModel> {
            override fun onResponse(call: Call<CityModel>, response: Response<CityModel>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<CityModel>, t: Throwable) {
                onResult(false, null)
            }

        })
    }

    fun getShippingCost(url: String, mBody: HashMap<String,String>, onResult: (isSuccess: Boolean, response: ShippingCostModel?) -> Unit) {
        ApiClient.instance.getShippingCost(url, mBody).enqueue(object: Callback<ShippingCostModel> {
            override fun onResponse(call: Call<ShippingCostModel>, response: Response<ShippingCostModel>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<ShippingCostModel>, t: Throwable) {
                onResult(false, null)
            }

        })
    }




}