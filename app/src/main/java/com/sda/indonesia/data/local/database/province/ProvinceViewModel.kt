package com.sda.indonesia.data.local.database.province

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ProvinceViewModel constructor(application: Application) : AndroidViewModel(application) {
    private val repository : ProvinceRepository = ProvinceRepository(application)
    private val data : LiveData<List<ProvinceEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<ProvinceEntity>> {
        return data
    }

    fun insert(data: ProvinceEntity){
        repository.insert(data)
    }

//    fun upsert(data: KelasEntity){
//        repository.upsert(data)
//    }

    fun deleteAll(){
        repository.deleteAll()
    }

}