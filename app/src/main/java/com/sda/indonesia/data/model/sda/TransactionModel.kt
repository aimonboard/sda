package com.sda.indonesia.data.model.sda

data class TransactionModel (
    val id: String,
    val status: String
)