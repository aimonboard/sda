package com.sda.indonesia.data.local.database.city

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CityDao {

    @Query("SELECT * from table_city")
    fun getAll(): LiveData<List<CityEntity>>

    @Query("SELECT * from table_city WHERE city_id =:uom_id")
    fun getById(uom_id: String): List<CityEntity>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(uom: CityEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(entity: CityEntity)

    @Delete
    fun delete(uom: CityEntity)

//    fun upsert(entity: CityEntity) {
//        try {
//            insert(entity)
//        } catch (exception: Exception) {
//            update(entity)
//        }
//    }


    @Query("DELETE FROM table_city")
    fun deleteAll()


}