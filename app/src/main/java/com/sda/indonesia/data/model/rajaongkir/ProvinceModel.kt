package com.sda.indonesia.data.model.rajaongkir


import com.google.gson.annotations.SerializedName

data class ProvinceModel(
    @SerializedName("rajaongkir")
    val rajaongkir: Rajaongkir
) {
    data class Rajaongkir(
        @SerializedName("query")
        val query: List<Any>,
        @SerializedName("results")
        val results: List<Result>,
        @SerializedName("status")
        val status: Status
    ) {
        data class Result(
            @SerializedName("province")
            val province: String,
            @SerializedName("province_id")
            val provinceId: String
        )

        data class Status(
            @SerializedName("code")
            val code: Int,
            @SerializedName("description")
            val description: String
        )
    }
}