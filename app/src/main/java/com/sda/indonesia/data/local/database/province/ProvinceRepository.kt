package com.sda.indonesia.data.local.database.province

import android.app.Application
import androidx.lifecycle.LiveData

class ProvinceRepository constructor(application: Application) {

    lateinit var dao : ProvinceDao
    lateinit var data : LiveData<List<ProvinceEntity>>

    init {
        val database = ProvinceDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.getAll()
        }
    }

    fun getAll() : LiveData<List<ProvinceEntity>> {
        return data
    }

    fun insert(entity: ProvinceEntity) {
        val thread = object : Thread() {
            override fun run() {
                dao.insert(entity)
            }
        }
        thread.start()
    }

    fun deleteAll() {
        val thread = object : Thread() {
            override fun run() {
                dao.deleteAll()
            }
        }
        thread.start()
    }

}