package com.sda.indonesia.data.model.sda


import com.google.gson.annotations.SerializedName

data class LoginModel(
    @SerializedName("id")
    val id: String? = "",
    @SerializedName("nama")
    val nama: String? = "",
    @SerializedName("email")
    val email: String? = "",
    @SerializedName("success")
    val success: Boolean? = false,
    @SerializedName("message")
    val message: String? = "",
    @SerializedName("id_token")
    val idToken: String? = ""
)