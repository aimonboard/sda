package com.sda.indonesia.data.model.sda


import com.google.gson.annotations.SerializedName

data class ProductModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("http_status")
    val httpStatus: Int,
    @SerializedName("status")
    val status: Boolean
) {
    data class Data(
        @SerializedName("deskripsi_produk")
        val deskripsiProduk: String,
        @SerializedName("id_produk")
        val idProduk: String,
        @SerializedName("img_produk")
        val imgProduk: String,
        @SerializedName("img_brand")
        val imgBrand: String,
        @SerializedName("link_produk_download")
        val linkProdukDownload: String,
        @SerializedName("nama_brand")
        val namaBrand: String,
        @SerializedName("nama_kategori")
        val namaKategori: String,
        @SerializedName("nama_kategori_sub")
        val namaKategoriSub: String,
        @SerializedName("nama_produk")
        val namaProduk: String,
        @SerializedName("produk_type")
        val produkType: String
    )
}