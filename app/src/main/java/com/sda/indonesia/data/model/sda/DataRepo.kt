package com.sda.indonesia.data.model.sda


import com.google.gson.annotations.SerializedName

data class DataRepo(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val data: DataMain,
    @SerializedName("status")
    val status: Int
) {
    data class DataMain(
        @SerializedName("current_page")
        val currentPage: Int,
        @SerializedName("data")
        val dataList: List<Data>,
        @SerializedName("first_page_url")
        val firstPageUrl: String,
        @SerializedName("from")
        val from: Int,
        @SerializedName("last_page")
        val lastPage: Int,
        @SerializedName("last_page_url")
        val lastPageUrl: String,
        @SerializedName("next_page_url")
        val nextPageUrl: String,
        @SerializedName("path")
        val path: String,
        @SerializedName("per_page")
        val perPage: Int,
        @SerializedName("prev_page_url")
        val prevPageUrl: Any?,
        @SerializedName("to")
        val to: Int,
        @SerializedName("total")
        val total: Int
    ) {
        data class Data(
            @SerializedName("category")
            val category: Category,
            @SerializedName("created")
            val created: String,
            @SerializedName("id")
            val id: Int,
            @SerializedName("id_m_eca_blog_category")
            val idMEcaBlogCategory: Int,
            @SerializedName("id_user")
            val idUser: Int,
            @SerializedName("image")
            val image: String,
            @SerializedName("image_path")
            val imagePath: String,
            @SerializedName("keterangan")
            val keterangan: String,
            @SerializedName("nama")
            val nama: String,
            @SerializedName("publish_date")
            val publishDate: String,
            @SerializedName("slug")
            val slug: String,
            @SerializedName("status")
            val status: Any?,
            @SerializedName("type")
            val type: String,
            @SerializedName("user_type")
            val userType: String
        ) {
            data class Category(
                @SerializedName("id")
                val id: Int,
                @SerializedName("nama")
                val nama: String
            )
        }
    }
}