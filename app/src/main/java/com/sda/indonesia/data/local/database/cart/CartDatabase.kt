package com.sda.indonesia.data.local.database.cart

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [CartEntity::class],version = 6, exportSchema = false)
@TypeConverters(CartConverter::class)
abstract class CartDatabase : RoomDatabase() {

    abstract fun dao(): CartDao

    companion object {
        var instance: CartDatabase? = null
        fun getInstance(context: Context): CartDatabase? {
            if (instance == null) {
                synchronized(CartDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        CartDatabase::class.java, "table_cart")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return instance
        }
    }

}