package com.sda.indonesia.data.local.database.category

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CategoryDao {

    @Query("SELECT * from table_category")
    fun getAll(): LiveData<List<CategoryEntity>>

    @Query("SELECT * from table_category WHERE id =:uom_id")
    fun getById(uom_id: String): List<CategoryEntity>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(uom: CategoryEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(entity: CategoryEntity)

    @Delete
    fun delete(uom: CategoryEntity)

//    fun upsert(entity: CategoryEntity) {
//        try {
//            insert(entity)
//        } catch (exception: Exception) {
//            update(entity)
//        }
//    }


    @Query("DELETE FROM table_category")
    fun deleteAll()

}