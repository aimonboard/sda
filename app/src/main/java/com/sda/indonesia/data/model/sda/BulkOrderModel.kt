package com.sda.indonesia.data.model.sda

data class BulkOrderModel (
    val id: String,
    val name: String,
    val qty: String
)