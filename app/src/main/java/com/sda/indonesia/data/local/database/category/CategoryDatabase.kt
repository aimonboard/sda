package com.sda.indonesia.data.local.database.category

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database (entities = [CategoryEntity::class],version = 2, exportSchema = false)
abstract class CategoryDatabase : RoomDatabase() {

    abstract fun dao(): CategoryDao

    companion object {
        var instance: CategoryDatabase? = null
        fun getInstance(context: Context): CategoryDatabase? {
            if (instance == null) {
                synchronized(CategoryDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        CategoryDatabase::class.java, "table_category")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return instance
        }
    }
}