package com.sda.indonesia.data.model.sda

data class DeliveryModel(
    val productName: String,
    val subQty: Int,
    val subCode: String,
    val subPrice: Double
)