package com.sda.indonesia.data.model.sda


import com.google.gson.annotations.SerializedName

data class ProductSubModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("http_status")
    val httpStatus: Int,
    @SerializedName("status")
    val status: Boolean
) {
    data class Data(
        @SerializedName("code_produk")
        val codeProduk: String = "",
        @SerializedName("dash")
        val dash: String?,
        @SerializedName("harga_produk")
        val hargaProduk: Double = 0.0,
        @SerializedName("height")
        val height: String = "",
        @SerializedName("id_sub_produk")
        val idSubProduk: String = "",
        @SerializedName("length")
        val length: String?,
        @SerializedName("nama_sub_produk")
        val namaSubProduk: String = "",
        @SerializedName("nameheight")
        val nameheight: Any = Any(),
        @SerializedName("namelength")
        val namelength: String?,
        @SerializedName("namesize")
        val namesize: String?,
        @SerializedName("nameweight")
        val nameweight: String = "",
        @SerializedName("namewidth")
        val namewidth: Any = Any(),
        @SerializedName("pressure")
        val pressure: String?,
        @SerializedName("size")
        val size: String?,
        @SerializedName("stok_produk")
        val stokProduk: Int = 0,
        @SerializedName("weight")
        val weight: Int = 0,
        @SerializedName("width")
        val width: String = ""
    )
}