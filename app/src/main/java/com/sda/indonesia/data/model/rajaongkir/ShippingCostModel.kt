package com.sda.indonesia.data.model.rajaongkir


import com.google.gson.annotations.SerializedName

data class ShippingCostModel(
    @SerializedName("rajaongkir")
    val rajaongkir: Rajaongkir
) {
    data class Rajaongkir(
        @SerializedName("destination_details")
        val destinationDetails: DestinationDetails,
        @SerializedName("origin_details")
        val originDetails: OriginDetails,
        @SerializedName("query")
        val query: Query,
        @SerializedName("results")
        val results: List<Result>,
        @SerializedName("status")
        val status: Status
    ) {
        data class DestinationDetails(
            @SerializedName("city_id")
            val cityId: String,
            @SerializedName("city_name")
            val cityName: String,
            @SerializedName("postal_code")
            val postalCode: String,
            @SerializedName("province")
            val province: String,
            @SerializedName("province_id")
            val provinceId: String,
            @SerializedName("type")
            val type: String
        )

        data class OriginDetails(
            @SerializedName("city_id")
            val cityId: String,
            @SerializedName("city_name")
            val cityName: String,
            @SerializedName("postal_code")
            val postalCode: String,
            @SerializedName("province")
            val province: String,
            @SerializedName("province_id")
            val provinceId: String,
            @SerializedName("type")
            val type: String
        )

        data class Query(
            @SerializedName("courier")
            val courier: String,
            @SerializedName("destination")
            val destination: String,
            @SerializedName("origin")
            val origin: String,
            @SerializedName("weight")
            val weight: Int
        )

        data class Result(
            @SerializedName("code")
            val code: String,
            @SerializedName("costs")
            val costs: List<Data>,
            @SerializedName("name")
            val name: String
        ) {
            data class Data(
                @SerializedName("cost")
                val cost: List<Cost>,
                @SerializedName("description")
                val description: String,
                @SerializedName("service")
                val service: String
            ) {
                data class Cost(
                    @SerializedName("etd")
                    val etd: String,
                    @SerializedName("note")
                    val note: String,
                    @SerializedName("value")
                    val value: Double
                )
            }
        }

        data class Status(
            @SerializedName("code")
            val code: Int,
            @SerializedName("description")
            val description: String
        )
    }
}