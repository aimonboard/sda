package com.sda.indonesia.data.model.sda


import com.google.gson.annotations.SerializedName

data class CategoryModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("http_status")
    val httpStatus: Int,
    @SerializedName("status")
    val status: Boolean
) {
    data class Data(
        @SerializedName("id_kategori")
        val idKategori: String,
        @SerializedName("img_icon")
        val imgIcon: String,
        @SerializedName("nama_kategori")
        val namaKategori: String
    )
}